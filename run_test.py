"""Experimentation"""
import os
import subprocess
from typing import List, Dict

csp: bool = False
cnf: bool = False
solve: bool = True

# Parameters
encoding_to_test: List[str] = ["prefixes", "suffixes", "bothGA", "direct"]
benchs_repository: str = "Benchs"
# states_to_test: List[int] = [3, 4]

states_for_bench: Dict[str, List[int]] = {"st-2-100.txt": [8, 9, 10], "st-2-10.txt": [3, 4, 5], "st-2-20.txt": [6, 7, 8],
                                          "st-2-30.txt": [8, 9, 10], "st-2-40.txt": [8, 9, 10], "st-2-50.txt": [8, 9, 10],
                                          "st-2-60.txt": [8, 9, 10], "st-2-70.txt": [8, 9, 10], "st-2-80.txt": [8, 9, 10],
                                          "st-2-90.txt": [8, 9, 10], "st-5-100.txt": [7, 8, 9], "st-5-10.txt": [2, 3, 4],
                                          "st-5-20.txt": [3, 4 ,5], "st-5-30.txt": [3, 4 ,5], "st-5-40.txt": [4, 5, 6],
                                          "st-5-50.txt": [5, 6, 7], "st-5-60.txt": [5, 6, 7], "st-5-70.txt": [6, 7, 8],
                                          "st-5-80.txt": [6, 7, 8], "st-5-90.txt": [6, 7, 8], "ww-10-10.txt": [2, 3, 4],
                                          "ww-10-20.txt": [2, 3, 4], "ww-10-30.txt": [2, 3, 4], "ww-10-40.txt": [3, 4, 5],
                                          "ww-10-50.txt": [3, 4, 5], "ww-10-60.txt": [4, 5, 6], "ww-10-70.txt": [4, 5, 6],
                                          "ww-10-80.txt": [4, 5, 6], "ww-10-90.txt": [4, 5, 6], "ww-10-99.txt": [4, 5, 6]}

# Tests
# benchs_to_test: List[str] = [f for f in os.listdir(benchs_repository) if os.path.isfile(os.path.join(benchs_repository, f))]
for encoding in encoding_to_test:
    # for state in states_to_test:
    #     for bench in benchs_to_test:
    for bench in states_for_bench.keys():
        for state in states_for_bench[bench]:
            out = " > " + benchs_repository + "/" + bench + "_" + str(state) + "_" + encoding + ".out"
            if cnf:
                cnf_file = " -cnf_file " + benchs_repository + "/" + bench + "_" + str(state) + "_" + encoding + ".cnf"
            else:
                cnf_file = ""
            if solve:
                algo = " -solver glucose4"
            else:
                algo = ""
            if csp:
                cspoption = " -csp True"
            else:
                cspoption = ""
            command_line = "python main.py -limit_solver 1200 -instance " + benchs_repository + "/" + bench + " -states " + str(state) + " -approach " + encoding + cspoption + cnf_file + algo + out
            print(command_line)
            subprocess.run(command_line, shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
