from typing import Set, List, Dict

from SAMPLE import SAMPLE


class NFACNF:
    """Class representing an NFA"""

    states: Set[int]
    """Set of states for the automata"""
    nb_cnf_var: int
    """Counter for CNF variables"""
    final_states: List[int]
    """List of CNF Boolean variables corresponding to the fact a state is final or not"""
    transitions: List[List[List[int]]]
    """3 dimensions list providing the CNF Boolean variable corresponding to a transition from a state s1 to state 
    s2 when reading character c.
    Usage: transitions[c][s1][s2]"""

    def __init__(self):
        """Constructor for SAMPLE class"""
        # set of states
        self.states = set()
        # cnf var
        self.nb_cnf_var = 0
        #  Final states
        self.final_states = []

    def init(self, sample: SAMPLE, nb_state: int, kp1: int = 0):
        """Constructor for SAMPLE class"""
        # set of states
        for x in range(nb_state):
            self.states.add(x)
        #  Final states
        if kp1 == 0:
            for _ in self.states:
                self.final_states.append(self.new_cnf_var())
        # Transitions
        self.transitions = [[[self.new_cnf_var() for _ in self.states]
                             for _ in self.states] for _ in sample.get_alphabet()]

    def new_cnf_var(self) -> int:
        """
        Provide the smallest unused CNF variable number.
        :return: int
        """
        self.nb_cnf_var += 1
        return self.nb_cnf_var
