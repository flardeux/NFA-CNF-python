if [ $# = 0 ]
then
  echo "Usage: $0 PNG_files_for_model1 PNG_files_for_model2"
  exit 0
fi

if [ `expr $# % 2` = 0 ]
then
  nb_file=$#
  nb_file=$((${nb_file} / 2))
  #echo "nb_file=" ${nb_file}
  for i in $(seq 1 $nb_file)
  do
    count="Erreur"
    for j in $(seq $((${nb_file}+1)) $#)
    do
      n=$(diff ${@:$i:1} ${@:$j:1})
      if test -z "$n"
      then
        echo "Automata ${@:$i:1} corresponds to ${@:$j:1}"
        count=$n
      fi
    done
    if test -n "$count"
    then
        echo "Automata ${@:$i:1} is not present in the second part of files."
    fi
  done
else
    echo "Wrong number of files"
fi