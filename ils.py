import random
from typing import List

import SAMPLE
import variables
import tools


def best_position(my_sample: SAMPLE, aff: List[int], word: int, bv: int, evaluation_function_to_use) -> [int, int]:
    """Return the best position for a given word to split between prefix and suffix. """
    best_pos = aff[word]
    best_val = bv
    old_aff = aff.copy()
    for i in reversed(range(len(my_sample.get_all_words()[word]) + 1)):
        # print("avant")
        # tools.print_tree(variables.prefix_tree)
        # tools.print_tree(variables.suffix_tree)
        tools.update_prefix_tree(my_sample.get_all_words()[word], aff[word], i,
                                 variables.prefix_tree)
        tools.update_suffix_tree(my_sample.get_all_words()[word], aff[word], i,
                                 variables.suffix_tree)
        aff[word] = i
        # print("->", i)
        # tools.print_tree(variables.prefix_tree)
        # tools.print_tree(variables.suffix_tree)
        # print("=>")
        # print(aff)
        temp_eval = evaluation_function_to_use(aff, True)
        # print("<=")
        if temp_eval < best_val:
            best_val = temp_eval
            best_pos = i

    for i in range(len(old_aff)):  # restore the initial state of the prefix and suffix trees
        tools.update_prefix_tree(my_sample.get_all_words()[i], aff[i], old_aff[i],
                                 variables.prefix_tree)
        tools.update_suffix_tree(my_sample.get_all_words()[i], aff[i], old_aff[i],
                                 variables.suffix_tree)

    return [best_pos, best_val]


def first_position(my_sample: SAMPLE, affectation: List[int], word: int, bv: int, evaluation_function_to_use) -> [int,
                                                                                                                  int]:
    """Return the best position for a given word to split between prefix and suffix. """
    best_pos = affectation[word]
    best_val = bv
    positions: List[int] = [i for i in range(len(my_sample.get_all_words()[word]))]
    random.shuffle(positions)
    # print(positions)
    old_aff = affectation.copy()
    for i in positions:
        tools.update_prefix_tree(my_sample.get_all_words()[word], affectation[word], i,
                                 variables.prefix_tree)
        tools.update_suffix_tree(my_sample.get_all_words()[word], affectation[word], i,
                                 variables.suffix_tree)
        affectation[word] = i
        temp_eval = evaluation_function_to_use(affectation, True)
        if temp_eval < best_val:
            for j in range(len(old_aff)):  # restore the initial state of the prefix and suffix trees
                tools.update_prefix_tree(my_sample.get_all_words()[j], affectation[j], old_aff[j],
                                         variables.prefix_tree)
                tools.update_suffix_tree(my_sample.get_all_words()[j], affectation[j], old_aff[j],
                                         variables.suffix_tree)
            return [i, temp_eval]
    for i in range(len(old_aff)):  # restore the initial state of the prefix and suffix trees
        tools.update_prefix_tree(my_sample.get_all_words()[i], affectation[i], old_aff[i],
                                 variables.prefix_tree)
        tools.update_suffix_tree(my_sample.get_all_words()[i], affectation[i], old_aff[i],
                                 variables.suffix_tree)
    # print("=============la")
    return [best_pos, best_val]


def iterated_greedy_search(my_sample: SAMPLE, affectation: List[int], evaluation_function_to_use) -> [List[int], int]:
    best_value = evaluation_function_to_use(affectation, True)
    count_noise = 0
    best_affectation = affectation.copy()
    for r in range(variables.greedy_search_run):
        best_pos = [-1, -1]
        for i in range(len(affectation)):
            # print(i, ":", affectation)
            pos, value = best_position(my_sample, affectation.copy(), i, best_value, evaluation_function_to_use)
            # pos, value = first_position(my_sample, affectation.copy(), i, best_value, evaluation_function_to_use)
            # print("==>", best_pos)
            if value < best_value:
                best_pos = [i, pos]
                # print("=!>", best_pos)
                best_value = value
        # print("=!!>", best_pos)
        if best_pos[1] == -1:  # no improvement
            count_noise += 1
            if count_noise > variables.max_no_change_noise:
                break
            # print("Noise")
            for i in range(len(affectation)):
                old = affectation[i]
                affectation[i] = random.randint(1, len(my_sample.get_all_words()[i]) - 1)
                tools.update_prefix_tree(my_sample.get_all_words()[i], old, affectation[i],
                                         variables.prefix_tree)
                tools.update_suffix_tree(my_sample.get_all_words()[i], old, affectation[i],
                                         variables.suffix_tree)

        else:
            tools.update_prefix_tree(my_sample.get_all_words()[best_pos[0]], affectation[best_pos[0]], best_pos[1],
                                     variables.prefix_tree)
            tools.update_suffix_tree(my_sample.get_all_words()[best_pos[0]], affectation[best_pos[0]], best_pos[1],
                                     variables.suffix_tree)
            affectation[best_pos[0]] = best_pos[1]
            best_affectation = affectation.copy()
            # print(best_pos, best_value)
            # print(affectation)
    return [best_affectation, best_value]


def select_word() -> int:
    val: float = random.randint(0, 100000) / 100000
    for i in range(len(variables.word_probability)):
        if variables.word_probability[i] >= val:
            return i
    return len(variables.word_probability)-1


def iterated_walk_search(my_sample: SAMPLE, affectation: List[int], evaluation_function_to_use) -> [List[int], int]:
    tools.compute_word_probability(my_sample.get_all_words())
    best_value = evaluation_function_to_use(affectation, True)
    best_local_value = best_value
    variables.max_no_improvement = len(affectation)/2
    count_no_improvement = 0
    best_affectation = affectation.copy()
    count_imp = 0
    same_value = 0
    for r in range(variables.walk_search_run):
        i = select_word()
        pos, value = best_position(my_sample, affectation.copy(), i, best_local_value, evaluation_function_to_use)
        if value <= best_local_value:
            if value == best_local_value:
                same_value += 1
            else:
                same_value = 0
                tools.update_prefix_tree(my_sample.get_all_words()[i], affectation[i], pos, variables.prefix_tree)
                tools.update_suffix_tree(my_sample.get_all_words()[i], affectation[i], pos, variables.suffix_tree)
                affectation[i] = pos
                best_local_value = value
                if value < best_value:
                    # print("New best value:", best_value, ", step:", r)
                    best_affectation = affectation.copy()
                    best_value = value
                count_imp += 1
        else:  # no improvement
            same_value = 0
            count_no_improvement += 1
        if same_value > variables.max_same_value or count_no_improvement > variables.max_no_improvement:
            # print("Noise after", count_imp, " improvements", count_no_improvement, "no_improvements and", same_value, "same values.")
            count_no_improvement = 0
            count_imp = 0
            for i in range(len(affectation)):
                old = affectation[i]
                affectation[i] = random.randint(1, len(my_sample.get_all_words()[i]) - 1)
                tools.update_prefix_tree(my_sample.get_all_words()[i], old, affectation[i],
                                         variables.prefix_tree)
                tools.update_suffix_tree(my_sample.get_all_words()[i], old, affectation[i],
                                         variables.suffix_tree)
            best_local_value = evaluation_function_to_use(affectation, True)
    return [best_affectation, best_value]
