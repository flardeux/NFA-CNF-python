import re  # regex


class SAMPLE:
    """Class representing a SAMPLE instance"""

    def __init__(self):
        """Constructor for SAMPLE class"""
        self.__instance = ""
        """SAMPLE instance name"""
        self.__alphabet = set()
        """Alphabet used in the SAMPLE instance"""
        self.__positive_words = set()
        """Set of positive words"""
        self.__negative_words = set()
        """Set of negative words"""
        self.__all_words = list()
        """Set of all words"""
        self.__lambda_word = 0
        """Variable representing the fact the empty word is negative (-1), positive (1) or not present (0)"""

    def get_lambda_word(self) -> int:
        return self.__lambda_word

    # ERIC: lambda_word devrait peut être être un bool
    # ERIC à quoi ça sert? c'est pas fait dans load_instance?
    # FRED: lambda_word peut prendre 3 valeurs et par défaut, 0 veut dire que le mot vide n'est pas dans l'instance.
    def set_lambda_word(self, value: bool):
        if value:
            self.__lambda_word = 1
        else:
            self.__lambda_word = -1

    def get_instance_name(self) -> str:
        return self.__instance

    def set_instance_name(self, name: str):
        self.__instance = name

    # ERIC: le nom de l'instance est le nom du fichier? avec le path?
    # FRED: oui, le nom de l'instance est le nom du fichier avec le path.
    def load_instance(self, instance: str):
        self.__instance = instance
        f = open(instance, "r")
        lines = f.readlines()
        for line_item in lines:
            if len(line_item) != 0 and (line_item[0] == "+" or line_item[0] == "-"):  # accepted word
                l_clean = re.sub("[ \t]+", "", str(line_item))  # clean spaces and tabulations
                word = l_clean.strip('\n')  # remove the final '\n'
                is_a_positive_word = word[0] == '+'
                word = word[1:]  # the sign is removed
                for character in word:
                    self.__alphabet.add(int(character))
                if is_a_positive_word:
                    if len(word) == 0:  # empty positive word
                        self.__lambda_word = 1
                    else:
                        self.__positive_words.add(word)
                else:
                    if len(word) == 0:  # empty negative word
                        self.__lambda_word = -1
                    else:
                        self.__negative_words.add(word)
        if self.__positive_words.intersection(self.__negative_words):  # intersection is not empty
            print("Error: positive and negative sets of words have common words !!!")
            exit(1)
        self.__all_words = list(sorted(self.__positive_words.union(self.__negative_words)))

    def print_words(self):
        print("Positive words:")
        print(self.__positive_words)
        print("Negative words:")
        print(self.__negative_words)

    def print_alphabet(self):
        print("Alphabet:")
        print(self.__alphabet)

    # ERIC: est ce que ces fonctions d'accès sont nécessaires?
    # FRED: Généralement on fait toujours des set et get sur
    # les variables de classes et on rend ces variables privées (ce que je ne sais pas faire en Python !!)
    def get_alphabet(self) -> set():
        return self.__alphabet

    def get_positive_words(self) -> set():
        return self.__positive_words

    def get_negative_words(self) -> set():
        return self.__negative_words

    def get_all_words(self) -> list():
        return self.__all_words
