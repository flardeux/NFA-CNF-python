# Data structures
# Prefixes
from typing import Dict, List, Set, Any, Tuple

# INPUT
from pysat.formula import CNF

from NFACNF import NFACNF
from SAMPLE import SAMPLE
from tools import Tree


mandatory_parameters: List[str] = ["-instance", "-states", "-approach"]
"""Set of mandatory parameters"""
optional_parameters: List[str] = ["-verbosity", "-cnf_file", "-sol_file", "-model", "-model_with_automata",
                       "-all_models", "-nb_models", "-solver", "-csp", "-seed", "-limit_solver", "-limit_model", "-sym_break", "-reduced_eq", "-prob", "-one_final_state", "-model_with_blocks"]
"""Set of optional parameters"""
solver_names: List[str] = ['cadical', "glucose3", "glucose4", "lingeling", "maplechrono", "maplecm ", "maplesat", "minicard",
                "minisat22", "minisatgh"]
"""Set of possible solvers"""
parameters: Dict[str, Any] = dict()
"""Dictionary of parameters parsed in the command line"""

# CNF
formula: CNF = CNF()
"""Formula corresponding to an instance of CNF object"""

# Parse SAMPLE instance
sample: SAMPLE = SAMPLE()
"""Sample of positive and negative words"""

# NFA
nfa_cnf: NFACNF = NFACNF()
"""NFA represented as CNF"""

map_of_prefix: Dict[str, List[int]] = dict()
"""Maps each prefix from a final state to an intermediate state and provides the CNF variable"""
map_pref_words: Dict[str, Set[str]] = dict()
"""Map providing for each prefix P the set of words having as prefix a prefix of P"""
# Suffixes
map_of_suffix: Dict[str, List[int]] = dict()
"""Maps each suffix from a final state to an intermediate state and provides the CNF variable"""
map_suf_words: Dict[str, Set[str]] = dict()
"""Map providing for each suffix P the set of words having as suffix a suffix of P"""
set_of_best_suffix: Set[str] = set()
"""Set of suffix"""
set_of_best_prefix: Set[str] = set()
"""Set of prefix"""
set_of_words_without_suffix: Set[str] = set()
"""Set of words without their best suffix"""
set_of_words_without_prefix: Set[str] = set()
# """Set of words without their best prefix"""
# word_with_best_suffix: Dict[str, str] = dict()
# """dictionary associating the best suffix for each word"""
# word_with_best_prefix: Dict[str, str] = dict()
"""dictionary associating the best suffix for each word"""
suffix_group: Dict[str, int] = dict()
"""dictionary associating a suffix to the positive sample (+1), the negative sample (-1) or both (0)"""
prefix_group: Dict[str, int] = dict()
"""dictionary associating a prefix to the positive sample (+1), the negative sample (-1) or both (0)"""

best_combinations: List[Tuple[str, str]] = list()
"""Combinations of prefixes and suffixes covering all the words"""
word_with_best_suffix: Dict[str, str] = dict()
"""dictionary associating the best suffix for each word"""
word_with_best_prefix: Dict[str, str] = dict()
"""dictionary associating the best suffix for each word"""
word_with_best_combination: Dict[str, Tuple[str, str]] = dict()
"""dictionary associating the best prefix for each word"""
best_prefixes: Set[str] = set()
"""Set of all prefixes present in the best combinations"""
best_suffixes: Set[str] = set()
"""Set of all suffixes present in the best combinations"""

csp: bool = bool()
"""If True then CSP model is generated (name_instance-nb_states.xml)"""

time_without_writing: float = 0
"""Time to generate the model without the writing time"""

seed: int = -1
"""Seed for random functions"""

time_limit_model: int = 0
"""Time limit for the modeling process"""

time_limit_solver: int = 0
"""Time limit for the solving process"""

modelized: bool = False
"""Flag True if the model is done"""

csp_var = None

greedy_search_run: int = 10000
max_no_change_noise: int = 100

walk_search_run: int = 10000
max_same_value: int = 100
max_no_improvement: int = 10

prefix_tree: Tree = Tree("")
suffix_tree: Tree = Tree("")

word_probability: List[float] = list()
"""Associate a probability for each word with respect to its length"""
length_weight: float = 0.25

nb_evaluation_function_calls: int = 0

kp1: bool = False

possible_final_states: Dict[int, int] = dict()
"""Map for possible final state in kp1 model. [state][cnfvar]"""