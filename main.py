__author__ = "Frédéric Lardeux and Eric Monfroy"
__copyright__ = "Copyright 2021, The SAMPLE Project"
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Frédéric Lardeux"
__email__ = "frederic.lardeux@univ-angers.fr"
__status__ = "Development"

import subprocess
import sys
from multiprocessing import Process, Manager
import random
from threading import Timer
from typing import Set, List, Any, Dict, Tuple

import numpy as np
# import pycsp3
from matplotlib.pyplot import plot, show, legend

# import tools
from geneticalgorithm import geneticalgorithm as ga

import variables
import ils
import os

"""
Transformation of a SAMPLE instance to a CNF instance.
Possibility to solve the CNF instance and to generate all the models.

TODO: 
- both representation: clauses and variables number biggest than the other representation !!! 
"""

from SAMPLE import *  # Class representing an SAMPLE instance
from NFACNF import *  # Class representing an NFACNF instance

import output  # all output functions
from variables import *
from tools import *

from pysat.formula import CNF
from pysat.solvers import Solver
import time


def print_help():
    """
    Print the help for running the program.
    :return:
    """
    print("Usage:", sys.argv[0], "mandatory_parameters [optional_parameters]")
    print("Mandatory parameters are:", end=" ")
    print(mandatory_parameters)
    print("\t-instance (str): file with the initial SAMPLE instance")
    print("\t-states (int): number of states of the automata")
    print(
        "\t-approach (str): {Direct Model (DM), Prefix Model (PM), Suffix Model (SM), Best Prefix Model (PstarM), Best Suffix Model (SstarM), Hybrid Models (ILS, GA), ILS with Best Prefix Model (PstarM_ILS), ILS with Best Suffix Model (SstarM_ILS)} method to generate the CNF instance")
    print("Optional parameters are:", end=" ")
    print(optional_parameters)
    print("\t-prob (str): NFA or DFA  [default: NFA]")
    print("\t-one_final_state (bool): only one final state  [default: False]")
    print("\t-verbosity (int): [0-1]  [default: 0]")
    print("\t-cnf_file (str): file where the CNF formula is writen [default: stdout]")
    print("\t-sol_file (str): file where models are writen [default: stdout]")
    print("\t-model (bool): True/False return only one model [default: False]")
    print("\t-model_with_automata (str): file where the automata is writen in dot format [default: stdout]")
    print("\t-model_with_blocks (str): file where the automata is writen in block format [default: stdout]")
    print("\t-all_models (bool): True/False return all the models [default: False]")
    print("\t-nb_models (bool): True/False return all the number of models [default: False]")
    print("\t-solver (str): choose a solver in the list", solver_names)
    print("\t-csp (bool): True/False CSP model is written in file instance_name_states_approach.xml [default: False]")
    print("\t-seed (int): Seed for random functions in the genetic algorithm [default: pid]")
    print("\t-limit_model (int): Time limit for the model process (in second) [default: no limit]")
    print("\t-limit_solver (int): Time limit for the solving process (in second) [default: no limit]")
    print("\t-sym_break (int): Symmetry breaking [default: 0] (6 for lex-order)")
    print("\t-reduced_eq (bool): reduced equivalences in implications [default: False]")
    exit(1)


def read_parameters():
    """
    Parse the parameters of the command line.
    :return:
    """
    # Initialization
    count_mandatory = 0  # counter of parsed mandatory parameters
    parameters["-instance"] = "st-vs.txt"
    parameters["-states"] = 3
    parameters["-verbosity"] = 0
    parameters["-cnf_file"] = ""
    parameters["-sol_file"] = ""
    parameters["-model"] = "False"
    parameters["-nb_models"] = "False"
    parameters["-model_with_automata"] = ""
    parameters["-model_with_blocks"] = ""
    parameters["-all_models"] = "False"
    parameters["-csp"] = "False"
    parameters["-solver"] = ""
    parameters["-seed"] = -1
    parameters["-approach"] = "prefixes"
    parameters["-prob"] = "NFA"
    parameters["-limit_model"] = 0
    parameters["-limit_solver"] = 0
    parameters["-sym_break"] = 0
    parameters["-one_final_state"] = "False"
    parameters["-reduced_eq"] = "False"
    # Parsing
    if len(sys.argv) > 2:
        if len(sys.argv) % 2 != 0:
            for counter in range(1, len(sys.argv) - 1, 2):  # increment by 2 starting at 2
                if sys.argv[counter] in mandatory_parameters:
                    count_mandatory += 1
                    parameters[sys.argv[counter]] = sys.argv[counter + 1]
                elif sys.argv[counter] in optional_parameters:
                    parameters[sys.argv[counter]] = sys.argv[counter + 1]
                else:
                    print("Error in the command line. Parameter\"", sys.argv[counter], "\" is not a correct parameter.")
                    print_help()
        else:
            print("Error in the command line.")
            print_help()
    if len(sys.argv) > 1 and count_mandatory != len(mandatory_parameters):
        print("Error in the command line. A mandatory parameter is missing.")
        print_help()
    if parameters["-solver"] != "" and parameters["-solver"] not in solver_names:
        print("You must choose a solver (-solver option) in the list of solvers")
        print_help()
    elif (parameters["-all_models"] == "True" or parameters["-nb_models"] == "True" or parameters["-model"] == "True" or
          parameters["-model_with_automata"] != "" or parameters["-model_with_blocks"] != "") and parameters[
        "-solver"] not in solver_names:
        print("You must choose a solver (-solver option) to use the selected optional parameters")
        print_help()
    variables.time_limit_solver = int(parameters["-limit_solver"])
    variables.time_limit_model = int(parameters["-limit_model"])
    if parameters["-seed"] == -1:
        variables.seed = os.getpid()
        parameters["-seed"] == str(variables.seed)
    else:
        variables.seed = int(parameters["-seed"])
    # if len(sys.argv) == 1:
    #     print_help()


def generate_final_with_lambda(epsilon: int):
    if epsilon == 1:
        formula.append([nfa_cnf.final_states[0]])
    elif epsilon == -1:
        formula.append([-nfa_cnf.final_states[0]])


def generate_negative_words_for_prefix_representation():  # [1] (8) p.7 - ICTAI (7)
    """
    Generate all the clauses corresponding to the negative words for prefixes representation. Formula (8) p.7 of [1]
    [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference
    :return:
    """
    for word in sample.get_negative_words():
        for s in nfa_cnf.states:
            formula.append([-map_of_prefix.get(word)[s], -nfa_cnf.final_states[s]])


def kp1_generate_negative_words_for_prefix_representation():  # [1] (8) p.7 - ICTAI (7)
    """
    K+1 model: Since we consider only one final state, q_{k+1} , we can omit f_i Boolean variables.

    Generate all the clauses corresponding to the negative words for prefixes representation. Formula (8) p.7 of [1]
    [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference
    :return:
    """

    for word in sample.get_negative_words():
        formula.append([-map_of_prefix.get(word)[len(nfa_cnf.states) - 1]])


def kp1_generate_constraints_for_prefix_representation():
    """Force at least one state to be a possible final state for K nfa"""
    # New 2022 07 16
    clause: List[int] = list()
    for s in range(len(nfa_cnf.states) - 1):  # possible final states
        for word in sample.get_negative_words():  # a negative word cannot finish in a final state
            formula.append([-map_of_prefix.get(word)[s], -possible_final_states[s]])

        clause.clear()
        clause.append(-possible_final_states[s])
        for word in sample.get_positive_words():  # at least one positive word have to finish in a final state
            for intermediate in range(len(nfa_cnf.states) - 1):
                temp_var = nfa_cnf.new_cnf_var()
                formula.append([-temp_var, map_of_prefix.get(word[:-1])[intermediate]])
                formula.append([-temp_var, nfa_cnf.transitions[int(word[-1])][intermediate][len(nfa_cnf.states) - 1]])
                formula.append([-temp_var, nfa_cnf.transitions[int(word[-1])][intermediate][s]])
                formula.append([temp_var, -map_of_prefix.get(word[:-1])[intermediate],
                                -nfa_cnf.transitions[int(word[-1])][intermediate][len(nfa_cnf.states) - 1],
                                -nfa_cnf.transitions[int(word[-1])][intermediate][s]])
                clause.append(temp_var)
        formula.append(clause.copy())

    for word in sample.get_positive_words():
        clause.clear()
        for s in range(len(nfa_cnf.states) - 1):  # possible final states
            temp_var = nfa_cnf.new_cnf_var()
            formula.append([-temp_var, possible_final_states[s]])
            formula.append([-temp_var, map_of_prefix.get(word)[s]])
            formula.append([temp_var, -possible_final_states[s], -map_of_prefix.get(word)[s]])
            clause.append(temp_var)
        formula.append(clause.copy())


def create_tree_from_i_to_q(word: str, i: int, q: int, tree_of_paths: Tree = Tree("")):
    if len(word) != 0:
        if len(word) > 1:
            temp: List = []
            for s in nfa_cnf.states:
                temp_tree = Tree(nfa_cnf.transitions[int(word[0])][i][s])
                create_tree_from_i_to_q(word[1:], s, q, temp_tree)
                temp.append(temp_tree)
            tree_of_paths.children = temp
        else:  # len(word) = 1
            # print(int(word), i, q)
            temp_tree = Tree(nfa_cnf.transitions[int(word[0])][i][q])
            tree_of_paths.children.append(temp_tree)


def create_paths_from_0_to_q(word: str, s: int) -> List[Set[int]]:
    t: Tree = Tree("")
    list_of_paths: List[Set[int]] = list()
    create_tree_from_i_to_q(word, 0, s, t)
    compute_branches(t, list_of_paths)
    return list_of_paths


def generate_negative_words_for_direct_representation():  # [OLA] (7) p.4 - ICTAI (3)
    """
    Generate all the clauses corresponding to the negative words. Formula (7) p.4 of [OLA]
    :return:
    """
    for word in sample.get_negative_words():
        # print("word size =", len(word))
        for s in nfa_cnf.states:
            list_of_paths: List[Set[int]] = create_paths_from_0_to_q(word, s)
            for path in list_of_paths:
                clause: List[int] = list()
                for value in path:
                    clause.append(-value)
                clause.append(-nfa_cnf.final_states[s])
                formula.append(clause.copy())


def generate_positive_words_for_direct_representation():  # ICTAI (2)
    """
    Generate all the clauses corresponding to the positive words for direct representation. Formula (4, 5, 6) p.4 of [OLA]
    """
    # print(sample.get_positive_words())
    for word in sample.get_positive_words():
        aux = list()  # auxiliary variables
        for s in nfa_cnf.states:
            list_of_paths: List[Set[int]] = create_paths_from_0_to_q(word, s)
            for path in list_of_paths:
                aux_var = nfa_cnf.new_cnf_var()
                aux.append(aux_var)
                clause: List[int] = [aux_var, -nfa_cnf.final_states[s]]  # formula (5) of [OLA]
                for d in path:
                    formula.append([-aux_var, d])  # formula (4) of [OLA]
                    clause.append(-d)  # formula (5) of [OLA]
                formula.append([-aux_var, nfa_cnf.final_states[s]])  # formula (4) of [OLA]
                formula.append(clause)  # formula (5) of [OLA]
        formula.append(aux)  # formula (6) of [OLA]


def generate_negative_words_for_suffix_representation(b_suffixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the negative words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that no combination
    of prefix and suffix finish in a final state :return:
    """
    for word in sample.get_negative_words():
        # print(b_suffixes.get(word))
        # suf: str = word_with_best_suffix.get(word)
        suf: str = b_suffixes.get(word)
        if len(suf) < len(word):
            if len(suf) != 0:
                for s_intermediate in nfa_cnf.states:
                    for s_final in nfa_cnf.states:
                        formula.append([-map_of_prefix.get(word[:-len(suf)])[s_intermediate],
                                        -get_map_of_suffix(suf, s_intermediate, s_final),
                                        -nfa_cnf.final_states[s_final]])
                        if csp:
                            satisfy(
                                variables.csp_var[map_of_prefix.get(word[:-len(suf)])[s_intermediate] - 1]
                                * variables.csp_var[get_map_of_suffix(suf, s_intermediate, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                == 0
                            )
            else:  # len(suf) == 0
                for s_final in nfa_cnf.states:
                    formula.append([-map_of_prefix.get(word)[s_final],
                                    -nfa_cnf.final_states[s_final]])
                    if csp:
                        satisfy(
                            variables.csp_var[map_of_prefix.get(word)[s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            == 0
                        )
        else:  # len(suf) == len(word)
            for s_final in nfa_cnf.states:
                formula.append([-get_map_of_suffix(suf, 0, s_final),
                                -nfa_cnf.final_states[s_final]])
                if csp:
                    satisfy(
                        variables.csp_var[get_map_of_suffix(suf, 0, s_final) - 1]
                        * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                        == 0
                    )


def kp1_generate_negative_words_for_suffix_representation(b_suffixes: Dict[str, str]):
    """
    k+1 -> only one final state

    Generate all the clauses corresponding to the negative words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that no combination
    of prefix and suffix finish in a final state :return:
    """
    for word in sample.get_negative_words():
        # print(b_suffixes.get(word))
        # suf: str = word_with_best_suffix.get(word)
        suf: str = b_suffixes.get(word)
        if len(suf) < len(word):
            if len(suf) != 0:
                for s_intermediate in range(len(nfa_cnf.states) - 1):
                    formula.append([-map_of_prefix.get(word[:-len(suf)])[s_intermediate],
                                    -map_of_suffix[suf][s_intermediate]])
            else:  # len(suf) == 0
                for s_final in [len(nfa_cnf.states) - 1]:  # k+1 -> only one final state
                    formula.append([-map_of_prefix.get(word)[s_final]])
        else:  # len(suf) == len(word)
            formula.append([-map_of_suffix[suf][0]])


def kp1_generate_constraints_for_suffix_representation(b_suffixes: Dict[str, str]):
    """Force at least one state to be a possible final state for K nfa"""
    # New 2022 07 16
    clause: List[int] = list()
    for s in range(len(nfa_cnf.states) - 1):  # possible final states
        for word in sample.get_negative_words():  # a negative word cannot finish in a final state
            suf: str = b_suffixes.get(word)
            if len(suf) < len(word):
                if len(suf) != 0:
                    for s_intermediate in range(len(nfa_cnf.states) - 1):
                        formula.append([-map_of_prefix.get(word[:-len(suf)])[s_intermediate],
                                        -map_of_suffix[suf][s_intermediate]])
                else:  # len(suf) == 0
                    for s_final in [len(nfa_cnf.states) - 1]:  # k+1 -> only one final state
                        formula.append([-map_of_prefix.get(word)[s_final]])
            else:  # len(suf) == len(word)
                formula.append([-map_of_suffix[suf][0], -possible_final_states[s]])

            formula.append([-map_of_prefix.get(word)[s], -possible_final_states[s]])

        clause.clear()
        clause.append(-possible_final_states[s])
        for word in sample.get_positive_words():  # at least one positive word have to finish in a final state
            for intermediate in range(len(nfa_cnf.states) - 1):
                temp_var = nfa_cnf.new_cnf_var()
                formula.append([-temp_var, map_of_prefix.get(word[:-1])[intermediate]])
                formula.append([-temp_var, nfa_cnf.transitions[int(word[-1])][intermediate][len(nfa_cnf.states) - 1]])
                formula.append([-temp_var, nfa_cnf.transitions[int(word[-1])][intermediate][s]])
                formula.append([temp_var, -map_of_prefix.get(word[:-1])[intermediate],
                                -nfa_cnf.transitions[int(word[-1])][intermediate][len(nfa_cnf.states) - 1],
                                -nfa_cnf.transitions[int(word[-1])][intermediate][s]])
                clause.append(temp_var)
        formula.append(clause.copy())

    for word in sample.get_positive_words():
        clause.clear()
        for s in range(len(nfa_cnf.states) - 1):  # possible final states
            temp_var = nfa_cnf.new_cnf_var()
            formula.append([-temp_var, possible_final_states[s]])
            formula.append([-temp_var, map_of_prefix.get(word)[s]])
            formula.append([temp_var, -possible_final_states[s], -map_of_prefix.get(word)[s]])
            clause.append(temp_var)
        formula.append(clause.copy())


def kp1_generate_negative_words_for_prefix_representation2(b_prefixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the negative words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that no combination
    of prefix and suffix finish in a final state :return:
    """
    for word in sample.get_negative_words():
        pref: str = b_prefixes.get(word)
        if len(pref) < len(word):
            if len(pref) != 0:
                for s_intermediate in range(len(nfa_cnf.states) - 1):
                    for s_final in [len(nfa_cnf.states) - 1]:
                        formula.append([-map_of_prefix.get(pref)[s_intermediate],
                                        -map_of_suffix[word[len(pref):]][s_intermediate]])
            else:  # len(pref) == 0
                formula.append([-map_of_suffix[word][0]])
        else:  # len(pref) == len(word)
            formula.append([-map_of_prefix.get(pref)[len(nfa_cnf.states) - 1]])


def generate_negative_words_for_prefix_representation2(b_prefixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the negative words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that no combination
    of prefix and suffix finish in a final state :return:
    """
    for word in sample.get_negative_words():
        pref: str = b_prefixes.get(word)
        if len(pref) < len(word):
            if len(pref) != 0:
                for s_intermediate in nfa_cnf.states:
                    for s_final in nfa_cnf.states:
                        formula.append([-map_of_prefix.get(pref)[s_intermediate],
                                        -get_map_of_suffix(word[len(pref):], s_intermediate, s_final),
                                        -nfa_cnf.final_states[s_final]])
                        if csp:
                            satisfy(
                                variables.csp_var[map_of_prefix.get(pref)[s_intermediate] - 1]
                                * variables.csp_var[get_map_of_suffix(word[len(pref):], s_intermediate, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                == 0
                            )
            else:  # len(pref) == 0
                for s_final in nfa_cnf.states:
                    formula.append([-get_map_of_suffix(word, 0, s_final),
                                    -nfa_cnf.final_states[s_final]])
                    if csp:
                        satisfy(
                            variables.csp_var[get_map_of_suffix(word, 0, s_final) - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            == 0
                        )
        else:  # len(pref) == len(word)
            for s_final in nfa_cnf.states:
                formula.append([-map_of_prefix.get(pref)[s_final],
                                -nfa_cnf.final_states[s_final]])
                if csp:
                    satisfy(
                        variables.csp_var[map_of_prefix.get(pref)[s_final] - 1]
                        * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                        == 0
                    )


def generate_positive_words_for_suffix_representation(b_suffixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the positive words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that at least one combination
    of prefix and suffix finish in a final state :return
    """
    # print(sample.get_positive_words())
    aux = list()  # auxiliary variables
    for w in sample.get_positive_words():
        aux.clear()
        suf = b_suffixes.get(w)
        # print("Positive Word", w, "decomposed in", w[:-len(suf)], "+", suf)
        if len(suf) != 0:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append(
                        [var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                else:
                    if len(suf) == len(w):
                        var_aux = nfa_cnf.new_cnf_var()
                        aux.append(var_aux)
                        formula.append([-var_aux, get_map_of_suffix(suf, 0, s_final)])
                        formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                        formula.append([var_aux, -get_map_of_suffix(suf, 0, s_final),
                                        -nfa_cnf.final_states[s_final]])
                    else:
                        for s_intermediate in nfa_cnf.states:
                            var_aux = nfa_cnf.new_cnf_var()
                            aux.append(var_aux)
                            formula.append([-var_aux, map_of_prefix.get(w[:-len(suf)])[s_intermediate]])
                            formula.append([-var_aux, get_map_of_suffix(suf, s_intermediate, s_final)])
                            formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                            formula.append([var_aux, -map_of_prefix.get(w[:-len(suf)])[s_intermediate],
                                            -get_map_of_suffix(suf, s_intermediate, s_final),
                                            -nfa_cnf.final_states[s_final]])
        else:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append(
                        [var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                else:
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, map_of_prefix.get(w)[s_final]])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append([var_aux, -map_of_prefix.get(w)[s_final],
                                    -nfa_cnf.final_states[s_final]])

        formula.append(aux.copy())

        if csp:
            if len(suf) != 0:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    if len(suf) == len(w):
                        satisfy(
                            Sum(
                                variables.csp_var[get_map_of_suffix(suf, 0, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
                    else:
                        satisfy(
                            Sum(
                                variables.csp_var[map_of_prefix.get(w[:-len(suf)])[s_intermediate] - 1]
                                * variables.csp_var[get_map_of_suffix(suf, s_intermediate, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_intermediate in nfa_cnf.states
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
            else:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    satisfy(
                        Sum(
                            variables.csp_var[map_of_prefix.get(w)[s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )


def kp1_generate_positive_words_for_suffix_representation(b_suffixes: Dict[str, str]):
    """
    k+1 -> only one final state

    Generate all the clauses corresponding to the positive words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that at least one combination
    of prefix and suffix finish in a final state :return
    """
    # print(sample.get_positive_words())
    aux = list()  # auxiliary variables

    for w in sample.get_positive_words():
        aux.clear()
        suf = b_suffixes.get(w)
        # print("map_of_prefix", map_of_prefix.get(suf))

        # print("Positive Word", w, "decomposed in", w[:-len(suf)], "+", suf)
        if len(suf) != 0:
            for s_final in [len(nfa_cnf.states) - 1]:  # k+1 -> only one final state (the last)
                if len(w) == 1:  # word of size 1
                    # print("ici1")
                    formula.append([nfa_cnf.transitions[int(w[0])][0][s_final]])
                else:
                    if len(suf) == len(w):
                        # print("ici2")
                        formula.append([map_of_suffix[suf][0]])
                    else:
                        # print("ici3")
                        for s_intermediate in range(len(nfa_cnf.states) - 1):
                            var_aux = nfa_cnf.new_cnf_var()
                            aux.append(var_aux)
                            formula.append([-var_aux, map_of_prefix.get(w[:-len(suf)])[s_intermediate]])
                            formula.append([-var_aux, map_of_suffix[suf][s_intermediate]])
                            formula.append([var_aux, -map_of_prefix.get(w[:-len(suf)])[s_intermediate],
                                            -map_of_suffix[suf][s_intermediate]])
                        formula.append(aux.copy())
        else:
            for s_final in [len(nfa_cnf.states) - 1]:  # k+1 -> only one final state (the last)
                if len(w) == 1:  # word of size 1
                    # print("ici4")
                    formula.append([nfa_cnf.transitions[int(w[0])][0][s_final]])
                else:
                    # print("ici5")
                    formula.append([map_of_prefix.get(w)[s_final]])


def generate_positive_words_for_suffix_representation_without_equivalence(
        b_suffixes: Dict[str, str]):  # remplacer equivalences par implications
    """
    Generate all the clauses corresponding to the positive words for suffixes representation.
    Each word is decomposed in prefix + suffix and then we have to assume that at least one combination
    of prefix and suffix finish in a final state :return
    """
    # print(sample.get_positive_words())
    aux = list()  # auxiliary variables
    for w in sample.get_positive_words():
        aux.clear()
        suf = b_suffixes.get(w)
        # print("Positive Word", w, "decomposed in", w[:-len(suf)], "+", suf)
        if len(suf) != 0:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    # var_aux = nfa_cnf.new_cnf_var()
                    # aux.append(var_aux)
                    # formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    # formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    # formula.append([var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                    aux.append(nfa_cnf.transitions[int(w[0])][0][s_final])
                    aux.append(nfa_cnf.final_states[s_final])
                else:
                    if len(suf) == len(w):
                        # var_aux = nfa_cnf.new_cnf_var()
                        # aux.append(var_aux)
                        # formula.append([-var_aux, get_map_of_suffix(suf, 0, s_final)])
                        # formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                        # formula.append([var_aux, -get_map_of_suffix(suf, 0, s_final), -nfa_cnf.final_states[s_final]])
                        aux.append(get_map_of_suffix(suf, 0, s_final))
                        aux.append(nfa_cnf.final_states[s_final])
                    else:
                        for s_intermediate in nfa_cnf.states:
                            # var_aux = nfa_cnf.new_cnf_var()
                            # aux.append(var_aux)
                            # formula.append([-var_aux, map_of_prefix.get(w[:-len(suf)])[s_intermediate]])
                            # formula.append([-var_aux, get_map_of_suffix(suf, s_intermediate, s_final)])
                            # formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                            # formula.append([var_aux, -map_of_prefix.get(w[:-len(suf)])[s_intermediate],
                            #                 -get_map_of_suffix(suf, s_intermediate, s_final),
                            #                 -nfa_cnf.final_states[s_final]])
                            aux.append(map_of_prefix.get(w[:-len(suf)])[s_intermediate])
                            aux.append(get_map_of_suffix(suf, s_intermediate, s_final))
                            aux.append(nfa_cnf.final_states[s_final])
        else:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    # var_aux = nfa_cnf.new_cnf_var()
                    # aux.append(var_aux)
                    # formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    # formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    # formula.append(
                    #     [var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                    aux.append(nfa_cnf.transitions[int(w[0])][0][s_final])
                    aux.append(nfa_cnf.final_states[s_final])
                else:
                    # var_aux = nfa_cnf.new_cnf_var()
                    # aux.append(var_aux)
                    # formula.append([-var_aux, map_of_prefix.get(w)[s_final]])
                    # formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    # formula.append([var_aux, -map_of_prefix.get(w)[s_final],
                    #                 -nfa_cnf.final_states[s_final]])
                    aux.append(map_of_prefix.get(w)[s_final])
                    aux.append(nfa_cnf.final_states[s_final])

        formula.append(aux.copy())

        if csp:
            if len(suf) != 0:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    if len(suf) == len(w):
                        satisfy(
                            Sum(
                                variables.csp_var[get_map_of_suffix(suf, 0, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
                    else:
                        satisfy(
                            Sum(
                                variables.csp_var[map_of_prefix.get(w[:-len(suf)])[s_intermediate] - 1]
                                * variables.csp_var[get_map_of_suffix(suf, s_intermediate, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_intermediate in nfa_cnf.states
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
            else:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    satisfy(
                        Sum(
                            variables.csp_var[map_of_prefix.get(w)[s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )


def generate_positive_words_for_prefix_representation2(b_prefixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the positive words for prefixes representation.
    Each word is decomposed in prefix + prefix and then we have to assume that at least one combination
    of prefix and prefix finish in a final state :return
    """
    # print(sample.get_positive_words())
    aux = list()  # auxiliary variables
    for w in sample.get_positive_words():
        aux.clear()
        pre = b_prefixes.get(w)
        # print("Positive Word", w, "decomposed in", w[:-len(pre)], "+", pre)
        if len(pre) != 0:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append(
                        [var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                else:
                    if len(pre) == len(w):
                        var_aux = nfa_cnf.new_cnf_var()
                        aux.append(var_aux)
                        formula.append([-var_aux, map_of_prefix.get(pre)[s_final]])
                        formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                        formula.append([var_aux, -map_of_prefix.get(pre)[s_final],
                                        -nfa_cnf.final_states[s_final]])
                    else:
                        for s_intermediate in nfa_cnf.states:
                            var_aux = nfa_cnf.new_cnf_var()
                            aux.append(var_aux)
                            formula.append([-var_aux, map_of_prefix.get(pre)[s_intermediate]])
                            formula.append([-var_aux, get_map_of_suffix(w[len(pre):], s_intermediate, s_final)])
                            formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                            formula.append([var_aux, -map_of_prefix.get(pre)[s_intermediate],
                                            -get_map_of_suffix(w[len(pre):], s_intermediate, s_final),
                                            -nfa_cnf.final_states[s_final]])
        else:
            for s_final in nfa_cnf.states:
                if len(w) == 1:  # word of size 1
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, nfa_cnf.transitions[int(w[0])][0][s_final]])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append(
                        [var_aux, -nfa_cnf.transitions[int(w[0])][0][s_final], -nfa_cnf.final_states[s_final]])
                else:
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    formula.append([-var_aux, get_map_of_suffix(w, 0, s_final)])
                    formula.append([-var_aux, nfa_cnf.final_states[s_final]])
                    formula.append([var_aux, -get_map_of_suffix(w, 0, s_final),
                                    -nfa_cnf.final_states[s_final]])

        formula.append(aux.copy())

        if csp:
            if len(pre) != 0:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    if len(pre) == len(w):
                        satisfy(
                            Sum(
                                variables.csp_var[map_of_prefix.get(pre)[s_final] - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
                    else:
                        satisfy(
                            Sum(
                                variables.csp_var[map_of_prefix.get(pre)[s_intermediate] - 1]
                                * variables.csp_var[get_map_of_suffix(w[len(pre):], s_intermediate, s_final) - 1]
                                * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                                for s_intermediate in nfa_cnf.states
                                for s_final in nfa_cnf.states
                            )
                            >= 1
                        )
            else:
                if len(w) == 1:  # word of size 1
                    satisfy(
                        Sum(
                            variables.csp_var[nfa_cnf.transitions[int(w[0])][0][s_final] - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )
                else:
                    satisfy(
                        Sum(
                            variables.csp_var[get_map_of_suffix(w, 0, s_final) - 1]
                            * variables.csp_var[nfa_cnf.final_states[s_final] - 1]
                            for s_final in nfa_cnf.states
                        )
                        >= 1
                    )


def kp1_generate_positive_words_for_prefix_representation2(b_prefixes: Dict[str, str]):
    """
    Generate all the clauses corresponding to the positive words for prefixes representation.
    Each word is decomposed in prefix + prefix and then we have to assume that at least one combination
    of prefix and prefix finish in a final state :return
    """
    # print(sample.get_positive_words())
    aux = list()  # auxiliary variables
    for w in sample.get_positive_words():
        aux.clear()
        pre = b_prefixes.get(w)
        # print("Positive Word", w, "decomposed in", w[:-len(pre)], "+", pre)
        if len(pre) != 0:
            for s_final in [len(nfa_cnf.states) - 1]:
                if len(w) == 1:  # word of size 1
                    formula.append([nfa_cnf.transitions[int(w[0])][0][s_final]])
                else:
                    if len(pre) == len(w):
                        formula.append([map_of_prefix.get(pre)[s_final]])
                    else:
                        for s_intermediate in range(len(nfa_cnf.states) - 1):
                            var_aux = nfa_cnf.new_cnf_var()
                            aux.append(var_aux)
                            formula.append([-var_aux, map_of_prefix.get(pre)[s_intermediate]])
                            formula.append([-var_aux, map_of_suffix[w[len(pre):]][s_intermediate]])
                            formula.append([var_aux, -map_of_prefix.get(pre)[s_intermediate],
                                            -map_of_suffix[w[len(pre):]][s_intermediate]])
                        formula.append(aux.copy())
        else:
            for s_final in [len(nfa_cnf.states) - 1]:
                if len(w) == 1:  # word of size 1
                    formula.append([nfa_cnf.transitions[int(w[0])][0][s_final]])
                else:
                    formula.append([map_of_suffix[w][0]])


def generate_positive_words_for_prefix_representation():  # [1] (7) p.7
    """
    Generate all the clauses corresponding to the positive words for prefixes representation. Formula (7) p.7 of [1]
    [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference
    :return:
    """
    aux = list()  # auxiliary variables
    for w in sample.get_positive_words():
        aux.clear()
        for s in nfa_cnf.states:
            var_aux = nfa_cnf.new_cnf_var()
            aux.append(var_aux)
            formula.append([-var_aux, map_of_prefix.get(w)[s]])
            formula.append([-var_aux, nfa_cnf.final_states[s]])
            formula.append([var_aux, -map_of_prefix.get(w)[s], -nfa_cnf.final_states[s]])
        formula.append(aux)
        if csp:
            satisfy(
                Sum(variables.csp_var[map_of_prefix.get(w)[s] - 1] * variables.csp_var[nfa_cnf.final_states[s] - 1]
                    for s in nfa_cnf.states) >= 1
            )


def kp1_generate_positive_words_for_prefix_representation():  # [1] (7) p.7
    """
    K+1 model: Since we consider only one final state, q_{k+1} , we can omit f_i Boolean variables.

    Generate all the clauses corresponding to the positive words for prefixes representation. Formula (7) p.7 of [1]
    [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference
    :return:
    """
    for w in sample.get_positive_words():
        formula.append([map_of_prefix.get(w)[len(nfa_cnf.states) - 1]])


def generate_recursive_representation_for_suffix_representation(set_of_suffix: Set[str]):  # [1] (9) p.7
    """
    Generate all the clauses corresponding to the recursive representation for suffixes representation. Formula (9)
    p.7 of [1] [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference :return:
    """
    aux = list()  # auxiliary variables
    # pref = str()  # word without the last character
    print(suffix_group)
    for s_init in nfa_cnf.states:
        for s_final in nfa_cnf.states:
            # for w_key, w_value in map_of_suffix.items():
            for w_key in set_of_suffix:
                if len(w_key) > 1:  # if word with a single character -> [1] (4) p.7
                    aux.clear()
                    suf = w_key[1:]  # all the characters except the last one
                    # print(parameters["-reduced_eq"], variables.suffix_group[suf], (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] >= 0) or (parameters["-reduced_eq"] == "False"), (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] <= 0) or (parameters["-reduced_eq"] == "False"))
                    for s_inter in nfa_cnf.states:
                        var_aux = nfa_cnf.new_cnf_var()
                        aux.append(var_aux)
                        if (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] >= 0) or (
                                parameters["-reduced_eq"] == "False"):
                            # print(suf, "ici")
                            formula.append([-var_aux, nfa_cnf.transitions[int(w_key[0])][s_init][s_inter]])
                            formula.append([-var_aux, get_map_of_suffix(suf, s_inter, s_final)])
                        if (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] <= 0) or (
                                parameters["-reduced_eq"] == "False"):
                            # print(suf, "la")
                            formula.append([var_aux, -nfa_cnf.transitions[int(w_key[0])][s_init][s_inter],
                                            -get_map_of_suffix(suf, s_inter, s_final)])
                        # print("formula size part1=", len(formula.clauses))
                        if csp:
                            satisfy(
                                variables.csp_var[get_map_of_suffix(w_key, s_init, s_final) - 1]
                                >= variables.csp_var[nfa_cnf.transitions[int(w_key[0])][s_init][s_inter] - 1]
                                * variables.csp_var[get_map_of_suffix(suf, s_inter, s_final) - 1]
                            )
                    # <-
                    if (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] <= 0) or (
                            parameters["-reduced_eq"] == "False"):
                        for s in nfa_cnf.states:
                            formula.append([get_map_of_suffix(w_key, s_init, s_final), -aux[s]])
                    # ->
                    if (parameters["-reduced_eq"] == "True" and variables.suffix_group[suf] >= 0) or (
                            parameters["-reduced_eq"] == "False"):
                        aux.append(-get_map_of_suffix(w_key, s_init, s_final))
                        formula.append(aux.copy())
                    if csp:
                        satisfy(
                            variables.csp_var[get_map_of_suffix(w_key, s_init, s_final) - 1]
                            <= Sum(
                                variables.csp_var[nfa_cnf.transitions[int(w_key[0])][s_init][s_inter] - 1]
                                * variables.csp_var[get_map_of_suffix(suf, s_inter, s_final) - 1]
                                for s_inter in nfa_cnf.states
                            )
                        )
                # else:
                #     print("w_key -> ", parameters["-reduced_eq"], variables.suffix_group[w_key])


def kp1_generate_recursive_representation_for_suffix_representation(set_of_suffix: Set[str]):  # [1] (9) p.7
    """
    k+1 -> only one initial state (the last) for suffix representation

    Generate all the clauses corresponding to the recursive representation for suffixes representation. Formula (9)
    p.7 of [1] [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference :return:
    """
    aux = list()  # auxiliary variables
    # pref = str()  # word without the last character
    # print(len(suffix_group))
    # print(set_of_suffix)
    for s_init in range(len(nfa_cnf.states) - 1):
        for s_final in [len(nfa_cnf.states) - 1]:  # only one final state (the last)
            # for w_key, w_value in map_of_suffix.items():
            for w_key in set_of_suffix:
                if len(w_key) > 1:  # if word with a single character -> [1] (4) p.7
                    aux.clear()
                    suf = w_key[1:]  # all the characters except the last one
                    for s_inter in range(len(nfa_cnf.states) - 1):
                        var_aux = nfa_cnf.new_cnf_var()
                        aux.append(var_aux)
                        formula.append([-var_aux, nfa_cnf.transitions[int(w_key[0])][s_init][s_inter]])
                        formula.append([-var_aux, map_of_suffix[suf][s_inter]])
                        formula.append([var_aux, -nfa_cnf.transitions[int(w_key[0])][s_init][s_inter],
                                        -map_of_suffix[suf][s_inter]])
                        # print("formula size part1=", len(formula.clauses))
                    # <-
                    for s in range(len(nfa_cnf.states) - 1):
                        formula.append([map_of_suffix[w_key][s_init], -aux[s]])
                    # ->
                    aux.append(-map_of_suffix[w_key][s_init])
                    formula.append(aux.copy())
                # else:
                #     print("w_key -> ", parameters["-reduced_eq"], variables.suffix_group[w_key])


def generate_recursive_representation_for_prefix_representation(set_of_words: Set[str]):  # [1] (9) p.7
    """
    Generate all the clauses corresponding to the recursive representation for prefixes representation. Formula (9)
    p.7 of [1] [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference :return:
    """
    aux = list()  # auxiliary variables
    # pref = str()  # word without the last character
    # for w_key, w_value in sorted(map_of_prefix.items(), key=lambda map_item: len(map_item[0])):
    # print(len(set_of_words))
    for w_key in sorted(set_of_words, key=lambda item: len(item)):
        if len(w_key) > 1:  # if word with a single character -> [1] (4) p.7
            for s1 in nfa_cnf.states:
                aux.clear()
                pref = w_key[0:-1]  # all the characters except the last one
                for s2 in nfa_cnf.states:
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    # <-
                    formula.append([-var_aux, map_of_prefix.get(pref)[s2]])
                    formula.append([-var_aux, nfa_cnf.transitions[int(w_key[-1:])][s2][s1]])
                    # ->
                    formula.append([var_aux, -map_of_prefix.get(pref)[s2],
                                    -nfa_cnf.transitions[int(w_key[-1])][s2][s1]])
                # <-
                for s in nfa_cnf.states:
                    formula.append([map_of_prefix.get(w_key)[s1], -aux[s]])
                # ->
                aux.append(-map_of_prefix.get(w_key)[s1])
                formula.append(aux.copy())


def kp1_generate_recursive_representation_for_prefix_representation(set_of_words: Set[str]):  # [1] (9) p.7
    """
    k+1 -> no transition from last state

    Generate all the clauses corresponding to the recursive representation for prefixes representation. Formula (9)
    p.7 of [1] [1]: T. Jastrzab, Z.J. Czech, W. Wieczorek / Parallel Algorithms for Minimal SAMPLE Inference :return:
    """
    aux = list()  # auxiliary variables
    # pref = str()  # word without the last character
    # for w_key, w_value in sorted(map_of_prefix.items(), key=lambda map_item: len(map_item[0])):
    for w_key in sorted(set_of_words, key=lambda item: len(item)):
        if len(w_key) > 1:  # if word with a single character -> [1] (4) p.7
            for s1 in nfa_cnf.states:
                aux.clear()
                pref = w_key[0:-1]  # all the characters except the last one
                for s2 in range(len(nfa_cnf.states) - 1):
                    # if variables.prefix_group[pref] <= 0 or parameters["-reduced_eq"] == "False":
                    var_aux = nfa_cnf.new_cnf_var()
                    aux.append(var_aux)
                    # <-
                    formula.append([-var_aux, map_of_prefix.get(pref)[s2]])
                    formula.append([-var_aux, nfa_cnf.transitions[int(w_key[-1:])][s2][s1]])
                    # ->
                    formula.append([var_aux, -map_of_prefix.get(pref)[s2],
                                    -nfa_cnf.transitions[int(w_key[-1])][s2][s1]])
                # <-
                for s in range(len(nfa_cnf.states) - 1):
                    formula.append([map_of_prefix.get(w_key)[s1], -aux[s]])
                # ->
                aux.append(-map_of_prefix.get(w_key)[s1])
                formula.append(aux.copy())


def symmetry_breaking():  # 8.1
    for w in sorted(sample.get_positive_words(), key=lambda map_item: -len(map_item)):
        if len(w) > 1:
            c = w[0]
            c = "0"
            print("Selected char:", c)
            formula.append([(nfa_cnf.transitions[int(c)][0][0]), nfa_cnf.transitions[int(c)][0][1]])
            # formula.append([(-nfa_cnf.transitions[int(c)][0][0]), -nfa_cnf.transitions[int(c)][0][1]])
            # for s in range(2, len(nfa_cnf.states)):
            #     formula.append([-nfa_cnf.transitions[int(c)][0][s]])
            break


def symmetry_breaking2():
    first_char: Dict[str, int] = dict()
    for w in sample.get_positive_words():
        if w[0] in first_char.keys():
            first_char[w[0]] += 1
        else:
            first_char.update([(w[0], 1)])
    for w, w_value in sorted(first_char.items(), key=lambda map_item: -map_item[1]):
        print("Selected char:", w)
        formula.append([(nfa_cnf.transitions[int(w)][0][0]), nfa_cnf.transitions[int(w)][0][1]])
        formula.append([(-nfa_cnf.transitions[int(w)][0][0]), -nfa_cnf.transitions[int(w)][0][1]])
        for s in range(2, len(nfa_cnf.states)):
            formula.append([-nfa_cnf.transitions[int(w)][0][s]])
        break


def symmetry_breaking3():  # 8.2
    first_char: Set[int] = set()
    clause: Set[int] = set()
    for w in sorted(sample.get_positive_words(), key=lambda map_item: -len(map_item)):
        if len(first_char) == len(nfa_cnf.states) - 1:
            break
        if len(w) > 0:
            c = int(w[0])
            if c not in first_char:
                first_char.add(c)
                print("Selected char:", c)
                # print(nfa_cnf.transitions)
                clause.clear()
                for i in range(0, len(first_char) + 1):
                    clause.add(nfa_cnf.transitions[c][0][i])
                formula.append(clause.copy())
                # print(clause)
                # print("First_char:", first_char)
                # for j in range(0, len(first_char)):
                #     for l in range(j+1, len(first_char)+1):
                # print(c, ":", j, "->", l, ";")
                # print("nfa_cnf.transitions[c][0][j]", nfa_cnf.transitions[c][0][j], ",")
                # print("nfa_cnf.transitions[c][0][l]]", nfa_cnf.transitions[c][0][l])
                # formula.append([-nfa_cnf.transitions[c][0][j], -nfa_cnf.transitions[c][0][l]])
                # print("ici")
                # for s in range(len(first_char)+1, len(nfa_cnf.states)):
                #     formula.append([-nfa_cnf.transitions[c][0][s]])


def symmetry_breaking4():  # 8.2
    first_char: Set[int] = set()
    clause: Set[int] = set()
    for w in sorted(sample.get_positive_words(), key=lambda map_item: -len(map_item)):
        if len(first_char) == len(nfa_cnf.states) - 1:
            break
        if len(w) > 0:
            c = int(w[0])
            if c not in first_char:
                first_char.add(c)
                print("Selected char:", c)
                # print(nfa_cnf.transitions)
                clause.clear()
                for i in range(0, len(first_char) + 1):
                    clause.add(nfa_cnf.transitions[c][0][i])
                formula.append(clause.copy())
                # print(clause)
                # print("First_char:", first_char)
                for j in range(0, len(first_char)):
                    for l in range(j + 1, len(first_char) + 1):
                        # print(c, ":", j, "->", l, ";")
                        # print("nfa_cnf.transitions[c][0][j]", nfa_cnf.transitions[c][0][j], ",")
                        # print("nfa_cnf.transitions[c][0][l]]", nfa_cnf.transitions[c][0][l])
                        formula.append([-nfa_cnf.transitions[c][0][j], -nfa_cnf.transitions[c][0][l]])
                        # print("ici")
                for s in range(len(first_char) + 1, len(nfa_cnf.states)):
                    formula.append([-nfa_cnf.transitions[c][0][s]])


def symmetry_breaking5():  # 8.2 + final states
    # first_char: Set[int] = set()
    # clause: Set[int] = set()
    # for w in sorted(sample.get_positive_words(), key=lambda map_item: -len(map_item)):
    #     if len(first_char) == len(nfa_cnf.states) - 1:
    #         break
    #     if len(w) > 0:
    #         c = int(w[0])
    #         if c not in first_char:
    #             first_char.add(c)
    #             print("Selected char:", c)
    #             clause.clear()
    #             for i in range(0, len(first_char)+1):
    #                 clause.add(nfa_cnf.transitions[c][0][i])
    #             formula.append(clause.copy())
    if len(nfa_cnf.states) > 2:
        formula.append([nfa_cnf.final_states[0], nfa_cnf.final_states[1], nfa_cnf.final_states[2]])


def evaluation_function(affectation: List[int], dynamic_tree: bool = False):
    variables.nb_evaluation_function_calls += 1
    pref: Set[str] = set()
    suff: Set[str] = set()
    my_map_of_prefix: Dict[str, List[int]] = dict()
    my_map_of_suffix: Dict[str, List[int]] = dict()
    all_words = sample.get_all_words()

    for i in range(len(affectation)):
        pref.add(all_words[i][:int(affectation[i])])
        suff.add(all_words[i][int(affectation[i]):])

    clean_set(pref, True)
    clean_set(suff, False)
    create_map_of_prefix_fake(pref, my_map_of_prefix)
    create_map_of_suffix_fake(suff, my_map_of_suffix)
    # print(my_map_of_prefix.keys())
    # print(my_map_of_suffix.keys())
    cost = len(nfa_cnf.states) + pow(len(nfa_cnf.states), 2) * len(sample.get_alphabet())
    # print("step 0:", cost)
    for w in my_map_of_prefix.keys():
        # create_map_of_prefix
        if len(w) > 1:
            cost += len(nfa_cnf.states)
    # print("step 1:", cost)
    if parameters["-approach"] != "prefixes":
        # print("my_map_of_suffix:", my_map_of_suffix)
        # print("suff:", sorted(suff))
        for w in my_map_of_suffix.keys():
            # create_map_of_suffix
            if len(w) > 1:
                cost += pow(len(nfa_cnf.states), 2)
        # print("step 2:", cost)
    for w in my_map_of_prefix.keys():
        # generate_recursive_representation_for_prefix_representation
        if len(w) > 1:
            cost += pow(len(nfa_cnf.states), 2)
    # print("step 3:", cost)
    if parameters["-approach"] != "prefixes":
        for w in my_map_of_suffix.keys():
            # generate_recursive_representation_for_suffix_representation
            if len(w) > 1:
                cost += pow(len(nfa_cnf.states), 3)
        # print("step 4:", cost)
    # generate_negative_words_for_suffix_representation -> 0 cnf variable created
    # generate_negative_words_for_prefix_representation -> 0 cnf variable created
    # print("step 5:", cost)
    if parameters["-approach"] != "prefixes":
        for w in sample.get_positive_words():
            # generate_positive_words_for_suffix_representation
            if affectation[all_words.index(w)] < len(w):
                if len(w) == 1:
                    cost += len(nfa_cnf.states)
                else:
                    cost += pow(len(nfa_cnf.states), 2)
            else:
                cost += len(nfa_cnf.states)
        # print("step 6:", cost)
    if parameters["-approach"] == "prefixes":
        for w in sample.get_positive_words():
            # generate_positive_words_for_prefix_representation
            cost += len(nfa_cnf.states)
        # print("step 7:", cost)
    return cost


def evaluation_function5(affectation: List[int], dynamic_tree: bool = False):
    variables.nb_evaluation_function_calls += 1
    pref: Set[str] = set()
    suff: Set[str] = set()
    my_map_of_prefix: Dict[str, List[int]] = dict()
    my_map_of_suffix: Dict[str, List[int]] = dict()
    all_words = sample.get_all_words()

    for i in range(len(affectation)):
        pref.add(all_words[i][:int(affectation[i])])
        suff.add(all_words[i][int(affectation[i]):])

    clean_set(pref, True)
    clean_set(suff, False)
    create_map_of_prefix_fake(pref, my_map_of_prefix)
    create_map_of_suffix_fake(suff, my_map_of_suffix)
    # print(my_map_of_prefix.keys())
    # print(my_map_of_suffix.keys())
    cost = len(nfa_cnf.states) + pow(len(nfa_cnf.states), 2) * len(sample.get_alphabet())
    # print("step 0:", cost)
    for w in my_map_of_prefix.keys():
        # create_map_of_prefix
        if len(w) > 1:
            cost += len(nfa_cnf.states)
    # print("step 1:", cost)
    if parameters["-approach"] != "prefixes":
        # print("my_map_of_suffix:", my_map_of_suffix)
        # print("suff:", sorted(suff))
        for w in my_map_of_suffix.keys():
            # create_map_of_suffix
            if len(w) > 1:
                cost += pow(len(nfa_cnf.states), 2)
        # print("step 2:", cost)
    return cost


def evaluation_function2(affectation: List[int], dynamic_tree: bool = False) -> int:
    """Based on a tree representation of the prefixes and the suffixes"""
    variables.nb_evaluation_function_calls += 1
    pref: Tree = Tree("")
    suff: Tree = Tree("")

    if dynamic_tree:
        pref = prefix_tree
        suff = suffix_tree
    else:
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], pref)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suff)
    # print_tree(pref)
    # print_tree(suff)
    return size_of_tree(pref) + len(nfa_cnf.states) * size_of_tree(suff)


def evaluation_function4(affectation: List[int], dynamic_tree: bool = False) -> int:
    """Based on a tree representation of the prefixes and the suffixes"""
    variables.nb_evaluation_function_calls += 1
    pref: Tree = Tree("")
    suff: Tree = Tree("")

    all_words = sample.get_all_words()
    if dynamic_tree:
        pref = prefix_tree
        suff = suffix_tree
    else:
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], pref)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suff)
    return size_of_tree(pref) + size_of_tree(suff)


def evaluation_function6(affectation: List[int], dynamic_tree: bool = False) -> int:
    """Based on a tree representation of the prefixes and the suffixes"""
    variables.nb_evaluation_function_calls += 1
    pref: Tree = Tree("")
    suff: Tree = Tree("")
    count_pref: int = 0
    count_suff: int = 0

    all_words = sample.get_all_words()
    if dynamic_tree:
        pref = prefix_tree
        suff = suffix_tree
    else:
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], pref)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suff)
            # print(all_words[i], ":", all_words[i][:int(affectation[i])], "|", all_words[i][int(affectation[i]):], "(", all_words[i][int(affectation[i]):][::-1], ")")

    # print("ici")
    # print_tree(pref)
    # print_tree(suff)
    for i in range(len(affectation)):
        # print(all_words[i], ":", all_words[i][:int(affectation[i])], "|", all_words[i][int(affectation[i]):])
        count_pref += give_occ_for_word(all_words[i][:int(affectation[i])], pref)
        count_suff += give_occ_for_word(all_words[i][int(affectation[i]):][::-1], suff)
    # print("la")
    return -(len(nfa_cnf.states) * count_suff + count_pref)
    # return -(count_suff + count_pref)


def evaluation_function3(affectation: List[int], dynamic_tree: bool = False) -> int:
    """Based on a tree representation of the prefixes and the suffixes with penalty for large prefixes"""
    variables.nb_evaluation_function_calls += 1
    cost: int = 0
    pref: Tree = Tree("")
    suff: Tree = Tree("")

    all_words = sample.get_all_words()
    if dynamic_tree:
        pref = prefix_tree
        suff = suffix_tree
    else:
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], pref)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suff)
    size_pref_tree: int = size_of_tree(pref)
    size_suf_tree: int = size_of_tree(suff)
    depth_pref_tree: int = depth_of_tree(pref)
    depth_suf_tree: int = depth_of_tree(suff)

    # # Travail sur les mots positifs a mettre dans SAMPLE
    # neg_pref_tree: Tree = Tree("")
    # neg_suf_tree: Tree = Tree("")
    # for word in sample.get_negative_words():
    #     insert_word_in_tree(word[:int(affectation[all_words.index(word)])], neg_pref_tree)
    #     insert_word_in_tree(word[int(affectation[all_words.index(word)]):], neg_suf_tree)
    # size_neg_pref_tree: int = size_of_tree(neg_pref_tree)
    # size_neg_suf_tree: int = size_of_tree(neg_suf_tree)

    # print_tree(suff)
    cost = depth_pref_tree * size_pref_tree + len(nfa_cnf.states) * len(nfa_cnf.states) * depth_suf_tree * size_suf_tree
    #    cost = -(pow(len(sample.get_alphabet())+1, depth_pref_tree) - size_pref_tree) + len(nfa_cnf.states) * (-(pow(len(sample.get_alphabet())+1, depth_suf_tree) - size_suf_tree))
    # penalty for positive words
    #   cost += len(nfa_cnf.states) * len(nfa_cnf.states) * (size_pref_tree - size_neg_pref_tree + size_suf_tree - size_neg_suf_tree)
    # print(pow(len(sample.get_alphabet())+1, depth_pref_tree), size_pref_tree, pow(len(sample.get_alphabet())+1, depth_suf_tree), size_suf_tree, cost)

    return cost


def is_dfa():
    clause: List[int] = list()
    for c in sample.get_alphabet():
        for s_start in nfa_cnf.states:
            for s_finish1 in nfa_cnf.states:
                for s_finish2 in nfa_cnf.states:
                    if s_finish2 > s_finish1:
                        formula.append([-nfa_cnf.transitions[int(c)][s_start][s_finish1],
                                        -nfa_cnf.transitions[int(c)][s_start][s_finish2]])


def only_one_final_state():
    """Impose that only the last state is a final state"""
    for s in range(len(nfa_cnf.states) - 1):
        formula.append([-nfa_cnf.final_states[s]])
    formula.append([nfa_cnf.final_states[len(nfa_cnf.states) - 1]])

    """No output transition for the final state"""
    for s in nfa_cnf.states:
        for c in sample.get_alphabet():
            formula.append([-nfa_cnf.transitions[int(c)][len(nfa_cnf.states) - 1][s]])


def kp1_duplicated_transitions():
    for c in sample.get_alphabet():
        for s in range(len(nfa_cnf.states) - 1):
            clause: List[int] = [-nfa_cnf.transitions[int(c)][s][len(nfa_cnf.states) - 1]]
            for s2 in range(len(nfa_cnf.states) - 1):
                clause.append(nfa_cnf.transitions[int(c)][s][s2])
            formula.append(clause.copy())
            clause.clear()


def force_final_state(list_of_states: List[int]):
    for s in list_of_states:
        formula.append([nfa_cnf.final_states[s]])


def lex_order():
    global_less: List[int] = list()
    global_eq: List[int] = list()
    bound: int = len(nfa_cnf.states) - 1
    if kp1:
        bound -= 1
    for i in range(1, bound):
        # for j in range(len(nfa_cnf.states)):
        j = i + 1
        global_less.clear()
        global_eq.clear()
        for c in sample.get_alphabet():
            for k in nfa_cnf.states:
                l = nfa_cnf.new_cnf_var()
                e = nfa_cnf.new_cnf_var()
                # less strict
                formula.append([-nfa_cnf.transitions[int(c)][i][k], -l])
                formula.append([nfa_cnf.transitions[int(c)][j][k], -l])
                formula.append([nfa_cnf.transitions[int(c)][i][k], -nfa_cnf.transitions[int(c)][j][k], l])
                # eq
                formula.append([nfa_cnf.transitions[int(c)][i][k], nfa_cnf.transitions[int(c)][j][k], e])
                formula.append([-nfa_cnf.transitions[int(c)][i][k], -nfa_cnf.transitions[int(c)][j][k], e])
                formula.append([-nfa_cnf.transitions[int(c)][i][k], nfa_cnf.transitions[int(c)][j][k], -e])
                formula.append([nfa_cnf.transitions[int(c)][i][k], -nfa_cnf.transitions[int(c)][j][k], -e])
                if len(global_eq) == 0:
                    global_eq.append(e)
                else:
                    ge = nfa_cnf.new_cnf_var()
                    global_eq.append(ge)
                    formula.append([-ge, global_eq[-2]])
                    formula.append([-ge, e])
                    formula.append([-e, -global_eq[-2], ge])
                if len(global_less) == 0:
                    global_less.append(l)
                else:
                    gl = nfa_cnf.new_cnf_var()
                    global_less.append(gl)
                    formula.append([-gl, global_less[-2], global_eq[-2]])
                    formula.append([-gl, global_less[-2], l])
                    formula.append([-global_less[-2], gl])
                    formula.append([gl, -global_eq[-2], -l])
        formula.append([global_less[-1]])


def compute_possible_final_states(pfs: Dict[int, int]):
    for s in range(len(nfa_cnf.states) - 1):
        pfs.update([(s, nfa_cnf.new_cnf_var())])
    # for c in sample.get_alphabet():
    #     for s in range(len(nfa_cnf.states)-1):
    #         for s2 in range(len(nfa_cnf.states) - 1):
    #             fs = nfa_cnf.new_cnf_var()
    #             formula.append([-nfa_cnf.transitions[c][s][len(nfa_cnf.states) - 1],
    #                             -nfa_cnf.transitions[c][s][s2],
    #                             fs])
    #             formula.append([nfa_cnf.transitions[c][s][len(nfa_cnf.states) - 1],
    #                             -fs])
    #             formula.append([nfa_cnf.transitions[c][s][s2],
    #                             -fs])
    #             pfs[s2] = fs


def generate_model():
    # Parse SAMPLE instance
    sample.load_instance(parameters["-instance"])
    """Sample of positive and negative words"""

    # NFA
    # nfa_cnf = NFACNF(sample, int(parameters["-states"]))
    if parameters["-approach"] in ["PM_kp1", "SM_kp1", "ILS_kp1", "PstarM_ILS_kp1", "SstarM_ILS_kp1", "PstarM_kp1",
                                   "SstarM_kp1"]:
        kp1 = 1
    else:
        kp1 = 0
    nfa_cnf.init(sample, int(parameters["-states"]), kp1)
    """NFA represented as CNF"""

    if parameters["-approach"] in ["PM_kp1", "SM_kp1", "ILS_kp1", "PstarM_ILS_kp1", "SstarM_ILS_kp1", "PstarM_kp1",
                                   "SstarM_kp1"]:
        compute_possible_final_states(possible_final_states)

    # test
    # Direct Model
    # Direct Model

    if parameters["-prob"] == "DFA":
        is_dfa()

    if parameters["-one_final_state"] == "True":
        only_one_final_state()
        kp1_duplicated_transitions()

    # force_final_state([1])

    if int(parameters["-sym_break"]) == 1:
        symmetry_breaking()
    elif int(parameters["-sym_break"]) == 2:
        symmetry_breaking2()
    elif int(parameters["-sym_break"]) == 3:
        symmetry_breaking3()
    elif int(parameters["-sym_break"]) == 4:
        symmetry_breaking4()
    elif int(parameters["-sym_break"]) == 5:
        symmetry_breaking5()
    elif int(parameters["-sym_break"]) == 6:
        lex_order()

    create_prefix_list()
    create_suffix_list()

    if parameters["-approach"] == "DM":
        # ----------------------- CNF construction --------------------------------------
        # create CSP variables
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # Negative words
        generate_negative_words_for_direct_representation()
        # Positive words
        generate_positive_words_for_direct_representation()

        temp_list: List[int] = list()
        for w in list(sorted(sample.get_positive_words().union(sample.get_negative_words()))):
            temp_list.append(len(w))
        print(temp_list)
        print("Best combinations:", evaluation_function3(temp_list))
    # Recursive Prefix Model
    elif parameters["-approach"] == "PM":
        # ----------------------- CNF construction --------------------------------------
        create_map_of_prefix(sample.get_positive_words().union(sample.get_negative_words()), map_of_prefix)
        # create CSP variables
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        # Negative words
        generate_negative_words_for_prefix_representation()
        # Positive words
        generate_positive_words_for_prefix_representation()

        temp_list: List[int] = list()
        for w in list(sorted(sample.get_positive_words().union(sample.get_negative_words()))):
            temp_list.append(len(w))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Prefix Model
    elif parameters["-approach"] == "PM_kp1":
        variables.kp1 = True
        # ----------------------- CNF construction --------------------------------------
        create_map_of_prefix(sample.get_positive_words().union(sample.get_negative_words()), map_of_prefix)
        kp1_duplicated_transitions()
        # create CSP variables
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        # Negative words
        kp1_generate_negative_words_for_prefix_representation()
        # Positive words
        kp1_generate_positive_words_for_prefix_representation()
        kp1_generate_constraints_for_prefix_representation()

        temp_list: List[int] = list()
        for w in list(sorted(sample.get_positive_words().union(sample.get_negative_words()))):
            temp_list.append(len(w))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Suffix Model
    elif parameters["-approach"] == "SM_kp1":
        variables.kp1 = True
        # ----------------------- CNF construction --------------------------------------
        kp1_create_map_of_suffix(sample.get_all_words(), map_of_suffix)
        for w in sample.get_all_words():
            word_with_best_suffix.update({w: w})
        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # print("map_of_suffix", map_of_suffix)
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        kp1_generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        kp1_generate_positive_words_for_suffix_representation(word_with_best_suffix)

        temp_list: List[int] = list()
        for w in sample.get_all_words():
            temp_list.append(len(w))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Suffix Model
    elif parameters["-approach"] == "SM":
        # ----------------------- CNF construction --------------------------------------
        create_map_of_suffix(sample.get_all_words(), map_of_suffix)
        for w in sample.get_all_words():
            word_with_best_suffix.update({w: w})
        # create CSP variables
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(
            word_with_best_suffix)

        temp_list: List[int] = list()
        for w in sample.get_all_words():
            temp_list.append(len(w))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Hybrid Best Suffixes Model
    elif parameters["-approach"] == "SstarM_kp1":
        variables.kp1 = True
        # ----------------------- CNF construction --------------------------------------
        kp1_create_map_of_suffix(set_of_best_suffix, map_of_suffix)
        create_map_of_prefix(set_of_words_without_suffix, map_of_prefix)
        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        kp1_generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        kp1_generate_positive_words_for_suffix_representation(word_with_best_suffix)

        temp_list: List[int] = list()
        for w, p in sorted(word_with_best_suffix.items()):
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Hybrid Best Suffixes Model
    elif parameters["-approach"] == "SstarM":
        # print("step 0:", nfa_cnf.nb_cnf_var)
        # create_suffix_list() ->mis en début de prog
        # ----------------------- CNF construction --------------------------------------
        create_map_of_suffix(set_of_best_suffix, map_of_suffix)
        create_map_of_prefix(set_of_words_without_suffix, map_of_prefix)
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # print("step 4 (generate_recursive_representation_for_suffix_representation):", nfa_cnf.nb_cnf_var)
        # print("Set of words without best suffixes:")
        # print(set_of_words_without_suffix)
        # print("Prefixes:", map_of_prefix)
        # print("Suffixes:", map_of_suffix)
        # print("Prefixes:", sorted(map_of_prefix.keys()), "\nSuffixes:", sorted(set_of_best_suffix))
        # Here we generate only the prefixes corresponding to  set_of_words_without_suffix
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        # print("step 3 (generate_recursive_representation_for_prefix_representation):", nfa_cnf.nb_cnf_var)
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)

        temp_list: List[int] = list()
        for w, p in sorted(word_with_best_suffix.items()):
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Hybrid Best Prefixes Model
    elif parameters["-approach"] == "PstarM_kp1":
        variables.kp1 = True
        # create_prefix_list() -> mis en début de prog
        # ----------------------- CNF construction --------------------------------------
        create_map_of_prefix(set_of_best_prefix, map_of_prefix)
        kp1_create_map_of_suffix(set_of_words_without_prefix, map_of_suffix)

        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        kp1_generate_negative_words_for_prefix_representation2(word_with_best_prefix)
        kp1_generate_positive_words_for_prefix_representation2(word_with_best_prefix)

        temp_list: List[int] = list()
        for w, p in sorted(word_with_best_prefix.items()):
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Recursive Hybrid Best Prefixes Model
    elif parameters["-approach"] == "PstarM":
        # create_prefix_list() -> mis en début de prog
        # ----------------------- CNF construction --------------------------------------
        create_map_of_prefix(set_of_best_prefix, map_of_prefix)
        create_map_of_suffix(set_of_words_without_prefix, map_of_suffix)
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_negative_words_for_prefix_representation2(word_with_best_prefix)
        generate_positive_words_for_prefix_representation2(word_with_best_prefix)

        temp_list: List[int] = list()
        for w, p in sorted(word_with_best_prefix.items()):
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function2(temp_list))
    # Prefixes/Suffixes approach
    elif parameters["-approach"] == "both":
        generate_map_pref_words()
        # print("Prefixes:", map_pref_words.keys())
        generate_map_suf_words()
        # print("Suffixes:", map_suf_words.keys())
        generate_word_best_prefix_suffix(word_with_best_combination)
        # print("Word with best combination (prefix/suffix):", word_with_best_combination)
        for val, couple in word_with_best_combination.items():
            best_prefixes.add(couple[0])
            best_suffixes.add(couple[1])
            word_with_best_suffix.update({val: couple[1]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # print("Prefixes:", sorted(best_prefixes), "\nSuffixes:", sorted(best_suffixes))
        # ----------------------- CNF construction --------------------------------------
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        # print("step 3 (generate_recursive_representation_for_prefix_representation):", nfa_cnf.nb_cnf_var)
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # print("step 4 (generate_recursive_representation_for_suffix_representation):", nfa_cnf.nb_cnf_var)
        # print("map_of_suffix:", sorted(map_of_suffix))
        # print("map_of_prefix:", sorted(map_of_prefix))
        # print("word_with_best_suffix:", word_with_best_suffix)
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # print("step 5 (generate_negative_words_for_suffix_representation):", nfa_cnf.nb_cnf_var)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)
        # print("step 6 (generate_positive_words_for_suffix_representation):", nfa_cnf.nb_cnf_var)

        temp_list: List[int] = list()
        for w, p in sorted(word_with_best_suffix.items()):
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function3(temp_list))
    # Genetic Algorithm - Recursive Hybrid Model
    elif parameters["-approach"] == "GA":
        # Genetic Algorithm
        list_for_ga: List[Tuple[int, int]] = list()
        list_of_words: List[str] = sample.get_all_words()
        for w in list_of_words:
            list_for_ga.append((1, len(w)))  # There is at least a prefix of size 1
        aff = np.array(list_for_ga)
        np.random.seed(variables.seed)
        algorithm_param = {'max_num_iteration': 3000, 'population_size': 100, 'mutation_probability': 0.05,
                           'elit_ratio': 0.01,
                           'crossover_probability': 0.5, 'parents_portion': 0.3, 'crossover_type': 'uniform',
                           'max_iteration_without_improv': 300}
        # algorithm_param = {'max_num_iteration': 3000, 'population_size': 100, 'mutation_probability': 0.1, 'elit_ratio': 0.01,
        #                    'crossover_probability': 0.5, 'parents_portion': 0.3, 'crossover_type': 'uniform',
        #                    'max_iteration_without_improv': 100}
        model = ga(function=evaluation_function2,
                   dimension=len(sample.get_positive_words().union(sample.get_negative_words())), variable_type='int',
                   variable_boundaries=aff, algorithm_parameters=algorithm_param, convergence_curve=False)
        model.run()

        # for val, couple in word_with_best_combination.items():
        for index in range(len(model.output_dict["variable"])):
            best_prefixes.add(list_of_words[index][:int(model.output_dict["variable"][index])])
            best_suffixes.add(list_of_words[index][int(model.output_dict["variable"][index]):])
            word_with_best_suffix.update(
                {list_of_words[index]: list_of_words[index][int(model.output_dict["variable"][index]):]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # print(list_of_words)
        # print("Prefixes:", best_prefixes, "\nSuffixes:", best_suffixes)
        # ----------------------- CNF construction --------------------------------------
        # print("+++step 0", nfa_cnf.nb_cnf_var)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        # print("+++step 3", nfa_cnf.nb_cnf_var)
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # print("+++step 4", nfa_cnf.nb_cnf_var)
        # print("map_of_suffix:", sorted(map_of_suffix))
        # print("map_of_prefix:", sorted(map_of_prefix))
        # print("word_with_best_prefix:", word_with_best_prefix)
        # print("word_with_best_suffix:", word_with_best_suffix)
        # print("list of words", list_of_words)
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # print("+++step 5", nfa_cnf.nb_cnf_var)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)
        # print("+++step 6", nfa_cnf.nb_cnf_var)

        temp_list: List[int] = list()
        for w, p in word_with_best_suffix.items():
            temp_list.append(len(w) - len(p))
        print(temp_list)
        print("Best combinations:", evaluation_function3(temp_list))
    # Iterated Local Search - Recursive Hybrid Model
    elif parameters["-approach"] == "IGS":
        affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        # affectation: List[int] = [2 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_greedy_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_IGS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Walk Search - Recursive Hybrid Model
    elif parameters["-approach"] == "ILS":
        affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        # affectation: List[int] = [2 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_ILS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Walk Search - Recursive Hybrid Model
    elif parameters["-approach"] == "ILS_kp1":
        variables.kp1 = True
        affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        # affectation: List[int] = [2 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        kp1_create_map_of_suffix(best_suffixes, map_of_suffix)
        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        kp1_generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        kp1_generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Walk Search - Recursive Hybrid Model
    elif parameters["-approach"] == "ILS_Impl":
        affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        # affectation: List[int] = [2 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_ILS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation_without_equivalence(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Local Search initialized by Best Prefixes
    elif parameters["-approach"] == "PstarM_ILS":
        # create_prefix_list() -> mis en debut de prog
        affectation: List[int] = [0 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            affectation[i] = len(word_with_best_prefix[all_words[i]])
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_PstarM_ILS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
        # Iterated Local Search initialized by Best Prefixes
    elif parameters["-approach"] == "PstarM_ILS_kp1":
        variables.kp1 = True
        # create_prefix_list() -> mis en debut de prog
        affectation: List[int] = [0 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(affectation)):
            affectation[i] = len(word_with_best_prefix[all_words[i]])
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        kp1_create_map_of_suffix(best_suffixes, map_of_suffix)
        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        kp1_generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        kp1_generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Local Search initialized by Best Suffixes
    elif parameters["-approach"] == "SstarM_ILS":
        # create_suffix_list() -> mis en début de prog
        # affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        affectation: List[int] = [0 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(sample.get_all_words())):
            affectation[i] = len(all_words[i]) - len(word_with_best_suffix[all_words[i]])
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        # word_with_best_suffix.clear()
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_SstarM_ILS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    # Iterated Local Search initialized by Best Suffixes
    elif parameters["-approach"] == "SstarM_ILS_kp1":
        variables.kp1 = True
        # create_suffix_list() -> mis en début de prog
        # affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        affectation: List[int] = [0 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(sample.get_all_words())):
            affectation[i] = len(all_words[i]) - len(word_with_best_suffix[all_words[i]])
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        # word_with_best_suffix.clear()
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        kp1_create_map_of_suffix(best_suffixes, map_of_suffix)
        # create_map_of_suffix(set_of_best_suffix, map_of_suffix)
        # create_map_of_prefix(set_of_words_without_suffix, map_of_prefix)
        kp1_duplicated_transitions()
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        kp1_generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        kp1_generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        kp1_generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        kp1_generate_positive_words_for_suffix_representation(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    elif parameters["-approach"] == "SstarM_ILS_Impl":
        # create_suffix_list() -> mis en début de prog
        # affectation: List[int] = [random.randint(0, len(w)) for w in sample.get_all_words()]
        affectation: List[int] = [0 for w in sample.get_all_words()]
        all_words = sample.get_all_words()
        for i in range(len(sample.get_all_words())):
            affectation[i] = len(all_words[i]) - len(word_with_best_suffix[all_words[i]])
            insert_word_in_tree(all_words[i][:int(affectation[i])], prefix_tree)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suffix_tree)
        affectation, value = ils.iterated_walk_search(sample, affectation, evaluation_function2)
        # word_with_best_suffix.clear()
        for index in range(len(affectation)):
            best_prefixes.add(sample.get_all_words()[index][:affectation[index]])
            best_suffixes.add(sample.get_all_words()[index][affectation[index]:])
            word_with_best_suffix.update(
                {sample.get_all_words()[index]: sample.get_all_words()[index][affectation[index]:]})
        clean_set(best_prefixes, True)
        clean_set(best_suffixes, False)
        # For all best prefixes and best suffixes, CNF variables are generated
        create_map_of_prefix(best_prefixes, map_of_prefix)
        create_map_of_suffix(best_suffixes, map_of_suffix)
        if csp:
            Compilation.user_filename = parameters["-instance"] + "_" + str(len(nfa_cnf.states)) + "_SstarM_ILS.xml"
            variables.csp_var = VarArray(size=nfa_cnf.nb_cnf_var, dom={0, 1})
        generate_final_with_lambda(sample.get_lambda_word())  # s_0=0 or 1 w.r.t. lambda
        # CNF variables are generated for recursive representation
        generate_recursive_representation_for_prefix_representation(set(map_of_prefix.keys()))
        generate_recursive_representation_for_suffix_representation(set(map_of_suffix.keys()))
        # Negative words
        generate_negative_words_for_suffix_representation(word_with_best_suffix)
        # Positive words
        # generate_positive_words_for_suffix_representation(word_with_best_suffix)
        generate_positive_words_for_suffix_representation_without_equivalence(word_with_best_suffix)

        print(affectation)
        print("Best combinations:", value)
    elif parameters["-approach"] == "test":
        affectation: List[int] = [2, 2, 2, 2, 1]
        print(sample.get_all_words())
        print(affectation)
        all_words = sample.get_all_words()
        pref: Tree = Tree("")
        suff: Tree = Tree("")
        for i in range(len(affectation)):
            insert_word_in_tree(all_words[i][:int(affectation[i])], pref)
            insert_word_in_tree(all_words[i][int(affectation[i]):][::-1], suff)
        print("pref tree")
        print_tree(pref)
        print("suff tree")
        print_tree(suff)
        update_prefix_tree(all_words[0], affectation[0], 3, pref)
        update_suffix_tree(all_words[0], affectation[0], 3, suff)
        print("pref tree")
        print_tree(pref)
        print("suff tree")
        print_tree(suff)
        # print(evaluation_function(affectation))
        # print(evaluation_function2(affectation))
        exit(0)
    elif parameters["-approach"] == "testGraph":
        affectation: List[int] = [len(w) for w in sample.get_all_words()]
        ef: List[(int, int, int, int, int)] = list()
        for _ in range(100):
            for i in range(len(affectation)):
                affectation[i] = random.randint(0, len(sample.get_all_words()[i]))
            ef.append((evaluation_function(affectation), evaluation_function2(affectation),
                       evaluation_function3(affectation), evaluation_function4(affectation),
                       evaluation_function5(affectation), evaluation_function6(affectation)))

        ef = (sorted(ef, key=lambda item: item[0]))
        ef1: List[int] = list()
        ef2: List[int] = list()
        ef3: List[int] = list()
        ef4: List[int] = list()
        ef5: List[int] = list()
        ef6: List[int] = list()
        for val in ef:
            ef1.append(val[0])
            ef2.append(val[1])
            ef3.append(val[2])
            ef4.append(val[3])
            ef5.append(val[4])
            ef6.append(val[5])
        plot(ef1, ef2, label="ef2")
        # plot(ef1, ef3, label="ef3")
        # plot(ef1, ef4, label="ef4")
        # plot(ef1, ef5, label="ef5")
        # plot(ef1, ef6, label="ef6")
        legend()
        show()
    else:
        print("No approach selected !")
        exit(1)

    # print_data()
    variables.time_without_writing = time.process_time()
    # CNF
    # output.print_cnf(sample, nfa_cnf, formula)
    if parameters["-cnf_file"] != "":
        output.print_cnf(sample, nfa_cnf, formula, open(parameters["-cnf_file"], 'w'))
    output.print_abstract(sample, formula)

    variables.modelized = True


def interrupt(s):
    """Interrupt the solver"""
    s.interrupt()


def stop(p: Process):
    p.close()


if __name__ == '__main__':

    # Read parameters line
    read_parameters()

    if parameters["-csp"] == "True":
        csp = True
    else:
        csp = False
    # csp = True
    if csp:
        print(parameters["-csp"])
        from pycsp3 import *  # satisfy, Sum, VarArray

    variables.modelized = False
    # if variables.time_limit_model != 0:
    #     generation_process = Process(target=generate_model)
    #     generation_process.start()
    #     generation_process.join(variables.time_limit_model)
    #     generation_process.terminate()
    #     print(formula.nv)
    # else:
    #     generate_model()
    generate_model()
    # print(sample.get_positive_words())
    # print(sample.get_negative_words())
    # Solver

    print(
        "Bench States Seed Approach Symetry Equiv. Vars Clauses Generation_Time Simp_Vars Simp_Clauses SAT Solving_time "
        "Restarts Conflicts Decisions Propagations Eval_function_calls")
    if variables.modelized:
        print(parameters["-instance"], parameters["-states"], variables.seed, parameters["-approach"],
              formula.nv, len(formula.clauses), variables.time_without_writing, "-", "-", "-", "-", "-", "-", "-",
              "-", variables.nb_evaluation_function_calls)
    else:
        print(parameters["-instance"], parameters["-states"], variables.seed, parameters["-approach"],
              parameters["-sym_break"], parameters["-reduced_eq"], "-",
              "-", variables.time_limit_model, "-", "-", "-", "-", "-", "-", "-", "-",
              variables.nb_evaluation_function_calls)
    if parameters["-solver"] != "":
        if variables.modelized:
            print("Solving Process")
            with Solver(name=parameters["-solver"], bootstrap_with=formula, use_timer=True) as g:
                if variables.time_limit_solver != 0:
                    timer = Timer(variables.time_limit_solver, interrupt, [g])
                    timer.start()
                    g.solve_limited(expect_interrupt=True)
                    timer.cancel()  # if solution found before the end of the time limit, the timer is canceled
                else:
                    g.solve()
                print(parameters["-instance"], parameters["-states"], variables.seed, parameters["-approach"],
                      parameters["-sym_break"], parameters["-reduced_eq"],
                      formula.nv,
                      len(formula.clauses), variables.time_without_writing, g.nof_vars(), g.nof_clauses(),
                      g.get_status(),
                      "{:f}".format(g.time()), g.accum_stats()["restarts"], g.accum_stats()["conflicts"],
                      g.accum_stats()["decisions"], g.accum_stats()["propagations"],
                      variables.nb_evaluation_function_calls)
                # count = 0
                # for m in g.enum_models():
                #     count += 1
                # print("Solutions number:", count)
                if parameters["-nb_models"] == "True":
                    output.print_nb_models(g)
                elif parameters["-all_models"] == "True" and parameters["-model_with_automata"] == "" and parameters[
                    "-model_with_blocks"] == "":
                    if parameters["-sol_file"] == "":
                        output.print_models(g)
                    else:
                        output.print_models_file(g, parameters["-sol_file"])
                elif parameters["-model"] == "True":
                    if parameters["-sol_file"] == "":
                        output.print_model(g)
                    else:
                        output.print_model(g, open(parameters["-sol_file"], 'w'))
                else:
                    if parameters["-all_models"] == "True":
                        counter = 0
                        for m in g.enum_models():
                            counter += 1
                            if parameters["-model_with_automata"] != "":
                                output.print_all_automata(sample, nfa_cnf, m, counter,
                                                          parameters["-model_with_automata"])
                            if parameters["-model_with_blocks"] != "":
                                output.print_all_automata_as_blocks(sample, nfa_cnf, m, counter,
                                                                    parameters["-model_with_blocks"])
                        print(counter, "different models were generated.")
                    else:
                        if parameters["-model_with_automata"] != "":
                            output.print_an_automata(sample, nfa_cnf, g, parameters["-model_with_automata"])
                        if parameters["-model_with_blocks"] != "":
                            output.print_an_automata_as_blocks(sample, nfa_cnf, g, parameters["-model_with_blocks"])
        # print_transitions()
    # print('iter2')
    # print_transitions2()
    # t = Tree("")
    # insert_word_in_tree("premi", t)
    # insert_word_in_tree("preter", t)
    # insert_word_in_tree("prier", t)
    # print_tree(t)
    # print(size_of_tree(t))
    # print(depth_of_tree(t))
    # create_tree_from_i_to_q("010100", 0, 2, t)
    # branches: List[Set[int]] = list()
    # compute_branches(t, branches)
    # print(branches)
