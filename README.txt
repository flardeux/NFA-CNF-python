Official doc: https://pysathq.github.io/installation.html

Management of dependencies

If virtual environment is not installed:
    pip install virtualenv

Creation of the virtual environment:
    virtualenv -p python3 venv

Activation of the virtual environment:
    source venv/bin/activate

Dependencies installation:
    pip install -r requirements.txt

Have fun ...

How to use Git ?
1/ Only the first time: git clone <url>
2/ Current state: git status
3/ Each work must correspond to a branch. Create a new branch and initialize with the master branch files: git branch Name_of_the_new_branch
4/ Copy the master branch to the branch: git pull origin master --allow-unrelated-histories
5/ Update the local repository: git pull
6/ Move to an other branch: git checkout Name_of_the_branch
7/ Add a new file: git add New_file
8/ Remove a file on the git repository: git rm File_to_remove
9/ Validate local modifications: git commit -a -m "Explanation of the modification"
10/ Transfer modifications from local repository to git repository: git push

Git push requires username and password:
A common cause is cloning using the default (HTTPS) instead of SSH. You can correct this by going to your repository, clicking "Clone or download",
then clicking the "Use SSH" button above the URL field and updating the URL of your origin remote like this:
git remote set-url origin git@github.com:username/repo.git

