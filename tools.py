from typing import List, Set, Dict, Tuple

import variables


class Tree:
    def __init__(self, data):
        self.children = []
        self.data = data
        self.occ = 1


def insert_word_in_tree(word: str, t: Tree):
    if len(word) > 0:
        for c in t.children:
            if c.data == word[0]:
                c.occ += 1
                insert_word_in_tree(word[1:], c)
                return
        temp: Tree = Tree(word[0])
        t.children.append(temp)
        insert_word_in_tree(word[1:], temp)


def give_occ_for_word(word: str, t: Tree) -> int:
    if len(word) == 0:
        return 0
    for c in t.children:
        if c.data == word[0]:
            if len(word) == 1:
                return c.occ
            else:
                return give_occ_for_word(word[1:], c)
    print("Error: word", word, "is not in the tree.")
    print_tree(t)
    exit(-1)


def print_tree(t: Tree, depth=None):
    if depth is None:
        depth = 0
    for c in t.children:
        for _ in range(depth):
            print("  ", end="")
        print(c.data, "(" + str(c.occ) + ")")
        print_tree(c, depth + 1)


def update_prefix_tree(word: str, sep_before: int, sep_after: int, t: Tree):
    if sep_before != sep_after:
        if sep_after < sep_before:  # reduce the prefix and increase the suffix
            if sep_after != 0:
                for i in range(sep_after):
                    for c in t.children:
                        if c.data == word[i]:
                            t = c
                            break
            cut: bool = False
            for i in range(sep_after, sep_before):
                if cut:
                    break
                else:
                    for c in t.children:
                        # print("->", word, i, sep_after, sep_before)
                        if c.data == word[i]:
                            if c.occ > 1:
                                c.occ -= 1
                                t = c
                                break
                            else:
                                t.children.remove(c)
                                cut = True
                                break
        else:  # reduce the suffix and increase the prefix
            for i in range(sep_before):
                for c in t.children:
                    if c.data == word[i]:
                        t = c
                        break
            for i in range(sep_before, sep_after):
                found: bool = False
                for c in t.children:
                    if c.data == word[i]:
                        c.occ += 1
                        t = c
                        found = True
                        break
                if not found:
                    # print("=>", word, i, sep_after, sep_before)
                    temp: Tree = Tree(word[i])
                    t.children.append(temp)
                    t = temp


def update_suffix_tree(word: str, sep_before: int, sep_after: int, t: Tree):
    if sep_after != sep_before:
        if sep_before < sep_after:  # reduce the suffix
            # if sep_before != 0:
            for i in range(len(word) - sep_after):
                for c in t.children:
                    if c.data == word[-(i + 1)]:
                        t = c
                        break
            cut: bool = False
            for i in range(len(word) - sep_after, len(word) - sep_before):
                if cut:
                    break
                else:
                    for c in t.children:
                        if c.data == word[-(i + 1)]:
                            if c.occ > 1:
                                c.occ -= 1
                                t = c
                                break
                            else:
                                t.children.remove(c)
                                cut = True
                                break
        else:  # reduce the suffix and increase the prefix
            for i in range(len(word) - sep_before):
                for c in t.children:
                    if c.data == word[-(i + 1)]:
                        t = c
                        break
            for i in range(len(word) - sep_before, len(word) - sep_after):
                found: bool = False
                for c in t.children:
                    if c.data == word[-(i + 1)]:
                        c.occ += 1
                        t = c
                        found = True
                        break
                if not found:
                    temp: Tree = Tree(word[-(i + 1)])
                    t.children.append(temp)
                    t = temp


def compute_branches(t: Tree, branches: List[Set[int]], s: Set[int] = set()):
    if t.data != "":
        s.add(t.data)
    if len(t.children) == 0:
        branches.append(s.copy())
    else:
        for c in t.children:
            compute_branches(c, branches, s.copy())


def size_of_tree(t: Tree) -> int:
    count: int = 0
    for c in t.children:
        count += 1 + size_of_tree(c)
    return count


def depth_of_tree(t: Tree) -> int:
    if len(t.children) == 0:
        return 0
    list_of_depth: List = list()
    for c in t.children:
        list_of_depth.append(1 + depth_of_tree(c))
    return max(list_of_depth)


def create_map_of_prefix(set_of_words: Set[str], my_map: Dict[str, List[int]]):
    """
    Analyse positive and negative words and complete the map of prefixes.
    :return:
    """
    # pref = str()  # prefix initialization
    set_of_prefix = set()
    for_states = list()
    for word in set_of_words:
        pref = ""  # clear the prefix variable
        for i in range(len(word)):  # all the prefixes of each word are build
            pref += word[i]  # by adding a character each time
            if pref not in set_of_prefix:  # if pref is a new prefix
                set_of_prefix.add(pref)  # add the prefix in set_of_prefix
                for_states.clear()
                for s in variables.nfa_cnf.states:
                    if i == 0:  # prefix of size 1 [1] (4) p.7 in create_map_of_prefix()
                        for_states.append(variables.nfa_cnf.transitions[int(word[i])][0][s])
                    else:
                        for_states.append(variables.nfa_cnf.new_cnf_var())
                # add the prefix associated to final state in map_of_prefix
                my_map.update([(pref, for_states.copy())])


def create_map_of_prefix_fake(set_of_words: Set[str], my_map: Dict[str, List[int]]):
    """
    Analyse positive and negative words and complete the map of prefixes.
    :return:
    """
    # pref = str()  # prefix initialization
    set_of_prefix = set()
    for_states = list()
    for word in set_of_words:
        pref = ""  # clear the prefix variable
        for i in range(len(word)):  # all the prefixes of each word are build
            pref += word[i]  # by adding a character each time
            if pref not in set_of_prefix:  # if pref is a new prefix
                set_of_prefix.add(pref)  # add the prefix in set_of_prefix
                for_states.clear()
                # add the prefix associated to final state in map_of_prefix
                my_map.update([(pref, for_states.copy())])


def get_map_of_suffix(suf: str, from_state: int, to_state: int) -> int:
    """
    Returns the Boolean CNF variable number generated by create_map_of_suffix.
    :param suf: The suffix
    :param from_state: Initial state
    :param to_state: Final state
    :return: Boolean CNF variable number
    """
    assert from_state < len(variables.nfa_cnf.states)
    assert to_state < len(variables.nfa_cnf.states)
    try:
        return variables.map_of_suffix.get(suf)[from_state * len(variables.nfa_cnf.states) + to_state]
    except IndexError:
        print("Parameters", from_state * len(variables.nfa_cnf.states) + to_state,"for get_map_suffix() function with ", suf, "are wrong. Suffix is not present in the dictionary.")
        exit(1)


def create_map_of_suffix(sob_suffix: Set[str], my_map: Dict[str, List[int]]):
    """
    Analyse positive and negative words and complete the map of suffixes.
    :return:
    """
    best_suffix_decomposition = set()
    for_states = list()

    # for word in set_of_best_suffix:
    for word in sob_suffix:
        # print("word=-"+word+"-")
        suffix = ""
        for i in reversed(range(len(word))):  # all the prefixes of each word are build
            suffix = word[i] + suffix  # by adding a character each time
            if suffix not in best_suffix_decomposition:  # if pref is a new prefix
                best_suffix_decomposition.add(suffix)  # add the suffix in best_suffix_decomposition
                for_states.clear()
                for s in variables.nfa_cnf.states:
                    for fs in variables.nfa_cnf.states:
                        if len(suffix) == 1:  # suffix of size 1 [1] (4) p.7 in create_map_of_suffix()
                            for_states.append(variables.nfa_cnf.transitions[int(word[-1:])][s][fs])
                        else:
                            for_states.append(variables.nfa_cnf.new_cnf_var())
                # add the suffix associated to final state in map_of_prefix
                my_map.update([(suffix, for_states.copy())])
                # print_map(map_of_suffix)


def kp1_create_map_of_suffix(sob_suffix: Set[str], my_map: Dict[str, List[int]]):
    """
    Analyse positive and negative words and complete the map of suffixes.
    :return:
    """
    best_suffix_decomposition = set()
    for_states = list()

    # for word in set_of_best_suffix:
    for word in sob_suffix:
        suffix = ""
        for i in reversed(range(len(word))):  # all the prefixes of each word are build
            suffix = word[i] + suffix  # by adding a character each time
            if suffix not in best_suffix_decomposition:  # if pref is a new prefix
                best_suffix_decomposition.add(suffix)  # add the suffix in best_suffix_decomposition
                for_states.clear()
                for s in range(len(variables.nfa_cnf.states)-1):
                    if len(suffix) == 1:  # suffix of size 1 [1] (4) p.7 in create_map_of_suffix()
                        for_states.append(variables.nfa_cnf.transitions[int(word[-1:])][s][len(variables.nfa_cnf.states)-1])
                    else:
                        for_states.append(variables.nfa_cnf.new_cnf_var())
                # add the suffix associated to final state in map_of_prefix
                my_map.update([(suffix, for_states.copy())])
                # print_map(map_of_suffix)


def create_map_of_suffix_fake(sob_suffix: Set[str], my_map: Dict[str, List[int]]):
    """
    Analyse positive and negative words and complete the map of suffixes.
    :return:
    """
    best_suffix_decomposition = set()
    for_states = list()

    # for word in set_of_best_suffix:
    for word in sob_suffix:
        suffix = ""
        for i in reversed(range(len(word))):  # all the prefixes of each word are build
            suffix = word[i] + suffix  # by adding a character each time
            if suffix not in best_suffix_decomposition:  # if pref is a new prefix
                best_suffix_decomposition.add(suffix)  # add the suffix in best_suffix_decomposition
                for_states.clear()
                # add the suffix associated to final state in map_of_prefix
                my_map.update([(suffix, for_states.copy())])
                # print_map(map_of_suffix)


def create_suffix_list():
    """
    Create the list of all possible suffixes with their occurrence numbers and the size of the
    words where they come from.
    :return:
    """
    set_of_suffix: Set[str] = set()
    set_of_suffix.add("")
    map_of_suffix_with_value: Dict[str, int] = dict()
    map_of_suffix_with_value.update([("", len(variables.sample.get_all_words()))])
    for word in variables.sample.get_positive_words():
        suf = ""  # clear the prefix variable
        for i in reversed(range(len(word))):  # all the suffixes of each word are build; -1 to remove suffixes
            # corresponding to the complete word
            suf = word[i] + suf  # by adding a character each time
            if suf not in set_of_suffix:  # if suf is a new suffix
                set_of_suffix.add(suf)  # add the suffix in set_of_suffix
                # add the suffix associated to final state in map_of_suffix
                map_of_suffix_with_value.update([(suf, 1)])
                variables.suffix_group.update([(suf, 1)]) # suffix is present in a word of the positive sample
            else:  # the suffix is already in the map so the counter is increased
                map_of_suffix_with_value[suf] = map_of_suffix_with_value[suf] + 1
    for word in variables.sample.get_negative_words():
        suf = ""  # clear the prefix variable
        for i in reversed(range(len(word))):  # all the suffixes of each word are build; -1 to remove suffixes
            # corresponding to the complete word
            suf = word[i] + suf  # by adding a character each time
            if suf not in set_of_suffix:  # if suf is a new suffix
                set_of_suffix.add(suf)  # add the suffix in set_of_suffix
                # add the suffix associated to final state in map_of_suffix
                map_of_suffix_with_value.update([(suf, 1)])
                variables.suffix_group.update([(suf, -1)]) # suffix is present in a word of the negative sample
            else:  # the suffix is already in the map so the counter is increased
                map_of_suffix_with_value[suf] = map_of_suffix_with_value[suf] + 1
                if variables.suffix_group[suf] == 1:
                    variables.suffix_group.update([(suf, 0)]) # suffix is present in words of the positive sample and the negative sample

    for word in variables.sample.get_all_words():
        for w_key, w_value in sorted(map_of_suffix_with_value.items(),
                                     # here the formula for sorting the suffixes
                                     key=lambda map_item: -(len(map_item[0]) * map_item[1])):
            if 0 < len(w_key) <= len(word) and w_key == word[-len(w_key):]:
                variables.word_with_best_suffix.update({word: w_key})
                variables.set_of_best_suffix.add(w_key)
                variables.set_of_words_without_suffix.add(word[:-len(w_key)])
                break


def create_prefix_list():
    """
    Create the list of all possible prefixes with their occurrence numbers and the size of the
    words where they come from.
    :return:
    """
    set_of_prefix = set()
    map_of_prefix_with_value: Dict[str, int] = dict()
    for word in variables.sample.get_positive_words():
        pref = ""  # clear the prefix variable
        for i in range(len(word)):  # all the prefixes of each word are build; -1 to remove suffixes
            # corresponding to the complete word
            pref = pref + word[i]  # by adding a character each time
            if pref not in set_of_prefix:  # if suf is a new prefix
                set_of_prefix.add(pref)  # add the prefix in set_of_suffix
                # add the prefix associated to final state in map_of_suffix
                map_of_prefix_with_value.update([(pref, 1)])
                variables.prefix_group.update([(pref, 1)])  # prefix is present in a word of the positive sample
            else:  # the prefix is already in the map so the counter is increased
                map_of_prefix_with_value[pref] = map_of_prefix_with_value[pref] + 1
    for word in variables.sample.get_negative_words():
        pref = ""  # clear the prefix variable
        for i in range(len(word)):  # all the prefixes of each word are build; -1 to remove suffixes
            # corresponding to the complete word
            pref = pref + word[i]  # by adding a character each time
            if pref not in set_of_prefix:  # if suf is a new prefix
                set_of_prefix.add(pref)  # add the prefix in set_of_suffix
                # add the prefix associated to final state in map_of_suffix
                map_of_prefix_with_value.update([(pref, 1)])
                variables.prefix_group.update([(pref, -1)])  # prefix is present in a word of the negative sample
            else:  # the prefix is already in the map so the counter is increased
                map_of_prefix_with_value[pref] = map_of_prefix_with_value[pref] + 1
                if variables.prefix_group[pref] == 1:
                    variables.prefix_group.update([(pref, 0)])  # prefix is present in words of the positive sample and the negative sample
    for word in variables.sample.get_all_words():
        for w_key, w_value in sorted(map_of_prefix_with_value.items(),
                                     # here the formula for sorting the prefixes
                                     key=lambda map_item: -(len(map_item[0]) * map_item[1])):
            if 0 < len(w_key) <= len(word) and w_key == word[:len(w_key)]:
                variables.word_with_best_prefix.update({word: w_key})
                variables.set_of_best_prefix.add(w_key)
                variables.set_of_words_without_prefix.add(word[len(w_key):])
                break


def generate_map_pref_words():
    """Map for each possible prefix all the words"""
    set_of_prefix: Set[str] = set()
    #  For each prefix the set of words is constructed
    for w in variables.sample.get_positive_words().union(variables.sample.get_negative_words()):
        pref: str = str()
        for car in w:
            pref = pref + car
            set_of_prefix.add(pref)
            if pref not in variables.map_pref_words.keys():
                variables.map_pref_words.update({pref: {w}})
            else:
                variables.map_pref_words.get(pref).add(w)
            if len(pref) > 1:
                variables.map_pref_words.get(pref).union(variables.map_pref_words.get(pref[:-1]))


def generate_map_suf_words():
    """Map for each possible prefix all the words"""
    set_of_suffix: Set[str] = set()
    #  For each suffix the set of words is constructed
    for w in variables.sample.get_positive_words().union(variables.sample.get_negative_words()):
        suf: str = str()
        for car in reversed(w):
            suf = car + suf
            set_of_suffix.add(suf)
            if suf not in variables.map_suf_words.keys():
                variables.map_suf_words.update({suf: {w}})
            else:
                variables.map_suf_words.get(suf).add(w)
            if len(suf) > 1:
                variables.map_suf_words.get(suf).union(variables.map_suf_words.get(suf[1:]))


def clean_set(b_set: Set[str], pref: bool):
    """Remove elements which are prefix (pref = True) of an other element. Same think but for suffix when pref =
    False """
    for w in sorted(b_set, reverse=True):
        for w2 in sorted(b_set, reverse=True):
            if len(w) > len(w2):
                if (pref and w.startswith(w2)) or (not pref and w.endswith(w2)):
                    b_set.remove(w2)


def corresponding_suffix(word: str, pref: str) -> str:
    """Return the shortest best suffix (map_suf_words) ending by the complementary of the prefix pref."""
    best_suf: str = word[len(pref):]
    for suf in sorted(variables.map_suf_words.keys(), key=lambda item: len(item)):
        if suf.endswith(best_suf):
            return suf
    return "!"  # No suffix correspond to the prefix


def prefix_cost(pref: str, w_pref_suf: Dict[str, Tuple[str, str]]) -> int:
    """Compute the cost of a prefix as the number of new tests to create.
    If a prefix a sub-prefix of an existing prefix, its cost is 0.
    Else its cost corresponds only to the part of the prefix not already present in the best prefixes.
    :param pref:
    :param w_pref_suf:"""
    cost: int = len(pref)
    for val_first, val_second in w_pref_suf.items():
        if str(val_second[0]).startswith(pref):
            return 0
        else:
            if pref.startswith(str(val_second[0])) and cost > len(pref) - len(str(val_second[0])):
                cost = len(pref) - len(str(val_second[0]))
    return cost


def suffix_cost(suf: str, w_pref_suf: Dict[str, Tuple[str, str]]) -> int:
    """Compute the cost of a prefix as the number of new tests to create.
    If a prefix is a sub-prefix of an existing prefix, its cost is 0.
    Else its cost corresponds only to the part of the prefix not already present in the best prefixes.
    :param suf:
    :param w_pref_suf:"""
    cost: int = len(suf)
    for val_first, val_second in w_pref_suf.items():
        if str(val_second[0]).endswith(suf):
            return 0
        else:
            if suf.endswith(str(val_second[0])) and cost > len(suf) - len(str(val_second[0])):
                cost = len(suf) - len(str(val_second[0]))
    return cost


def generate_word_best_prefix_suffix(w_pref_suf: Dict[str, Tuple[str, str]]):
    """Associate for each word the prefix and the suffix"""
    for pref, pref_words in sorted(variables.map_pref_words.items()):
        for word in pref_words:
            if len(pref) < len(word):
                suf = corresponding_suffix(word, pref)
                if suf != "!":
                    if word not in w_pref_suf.keys():
                        w_pref_suf.update({word: (pref, suf)})

                        # tradeoff between prefix size and suffix size
                    # elif (len(pref) + len(suf) - len(word) + 1) * len(pref) * len(suf) \
                    #      > (len(w_pref_suf[word][0]) + len(w_pref_suf[word][1]) - len(word) + 1) \
                    #      * len(w_pref_suf[word][0]) * len(w_pref_suf[word][1]):

                    elif (len(pref) + len(suf) - len(word) + 1) * prefix_cost(pref, w_pref_suf) \
                            * suffix_cost(suf, w_pref_suf) \
                            > (len(w_pref_suf[word][0]) + len(w_pref_suf[word][1]) - len(word) + 1) \
                            * prefix_cost(w_pref_suf[word][0], w_pref_suf) * suffix_cost(w_pref_suf[word][1],
                                                                                         w_pref_suf):
                        w_pref_suf[word] = (pref, suf)


def compute_word_probability(list_of_words: List[str]):
    sum_length: int = 0
    for word in list_of_words:
        sum_length += len(word)
    sum_proba: float = 0
    for word in list_of_words:
        sum_proba += (1-variables.length_weight)/len(list_of_words)+variables.length_weight*len(word)/sum_length
        variables.word_probability.append(sum_proba)

